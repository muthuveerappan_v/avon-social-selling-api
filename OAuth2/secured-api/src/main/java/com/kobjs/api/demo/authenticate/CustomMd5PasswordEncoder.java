package com.kobjs.api.demo.authenticate;

import java.security.MessageDigest;

import org.springframework.security.authentication.encoding.PasswordEncoder;

/**
 * @author rmohr
 * Provides a custom implementation of the Spring {@link PaswordEncoder}  interface to support the 
 * AVON MD5 hashing algorithm to maintain backward compatibility with passwords. 
 *
 */
@SuppressWarnings("deprecation")
public class CustomMd5PasswordEncoder implements PasswordEncoder {

	/* (non-Javadoc)
	 * Custom MD5 Password Encoder algorithm
	 * @see org.springframework.security.authentication.encoding.PasswordEncoder#encodePassword(java.lang.String, java.lang.Object)
	 */
	@Override
	public String encodePassword(String pwd, Object uid) {

		// Custom Implementation for AVON Password Hashing Algorithm
		MessageDigest md;
		StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(pwd.toString().getBytes("UTF-8"));
			byte[] md5 = md.digest();
			for (byte b : md5) {
				sb.append(String.format("%02x", b & 0xff));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	/* (non-Javadoc)
	 * Override Pasword Validation to use Custom MD5 Hash Algorithm
	 * @see org.springframework.security.authentication.encoding.PasswordEncoder#isPasswordValid(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public boolean isPasswordValid(String encodedPassword, String rawPassword, Object arg2) {	
		System.out.println("encodedPwd: " + encodedPassword + " rawPwd: " + rawPassword + " arg2: " + arg2);
		return this.encodePassword(rawPassword, null).equals(encodedPassword);
	}

}
