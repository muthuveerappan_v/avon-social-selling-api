package com.kobjs.api.demo.web;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.kobjs.api.demo.repository.CustomUserDetailsService;

@RestController
@EnableResourceServer
public class UserProfileController {

	protected final Log logger = LogFactory.getLog(getClass());
	 
	@Autowired
	RestTemplate restTemplate;

	@Autowired
    @Qualifier("tokenServices")
	DefaultTokenServices tokenServices;
	
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value="/api/user", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String helloUser(@RequestHeader("Authorization") String authHdr) {
    	
    	System.out.println("\n helloUser: " + authHdr + "\n");

    	// get Bearer token
    	String token = getBearerToken(authHdr);

        return "{ \"id\": \"" + this.getAttribute("acctNr", token) + "\", \"content\": \"" + token + "\" }";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value="/api/ags/secure", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	String setAGSToken(@RequestHeader("Authorization") String authHdr, @RequestBody String body) {
    	   	
    	System.out.println("\n setAGSToken: " + authHdr + "\n");
    	   	
    	// get Bearer token
    	String token = getBearerToken(authHdr);
    		
		return this.secureAGSBody(token, body); 

    }
    
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value="/api/profile/get", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	String postSecureAGSClassicRepProfile(@RequestHeader("Authorization") String authHdr, @RequestHeader(value="Content-Type", required=false) String contentTypeHdr, @RequestHeader(value="Accept", required=false) String acceptHdr,@RequestBody String body) {
    	
    	System.out.println("\n getProfile Authoriztion: " + authHdr + "\n");

    	String uri = "http://webeservicesqaf.avon.com/agws/srvc/repProfile/get";

    	// get Bearer token
    	String token = getBearerToken(authHdr);
    		
		String updatedBody = this.secureAGSBody(token, body); 

    			
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(updatedBody, headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.POST, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }

    @RequestMapping(value="/agsws/profile/get", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	String postAGSClassicRepProfile(@RequestHeader(value="Content-Type", required=false) String contentTypeHdr, @RequestHeader(value="Accept", required=false) String acceptHdr, @RequestBody String body) {
    	
    	System.out.println("\n getProfile postAGSClassicRepProfile: \n");

    	String uri = "http://webeservicesqaf.avon.com/agws/srvc/repProfile/get";
    			
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.POST, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }
    
    @RequestMapping(value="/agsws/login", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	String postAGSClassicLogin(@RequestHeader(value="Content-Type", required=false) String contentTypeHdr, @RequestHeader(value="Accept", required=false) String acceptHdr, @RequestBody String body) {
    	
    	System.out.println("\n PUBLIC postLogin: \n");

    	String uri = "https://webeservicesqaf.avon.com/agws/srvc/login";
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.POST, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }

    @RequestMapping(value="/ags3/ags-auth-web/rest/v1/{mrktId}/{langCd}/login", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	String postAGS3Login(@PathVariable(value="mrktId") final String mrktId, @PathVariable(value="langCd") final String langCd, @RequestHeader("devkey") String devKeyHdr, @RequestBody String body) {
    	
    	System.out.println("\n PUBLIC postAGS3Login: " + mrktId + "/" + langCd + "\n");

    	String uri = "http://webeservices3qaf.avon.com/ags-auth-web/rest/v1";
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		builder.pathSegment(mrktId, langCd, "login");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("devkey", devKeyHdr);
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.POST, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }

    @RequestMapping(value="/ags3/ags-profile-web/rest/v1/{mrktId}/{langCd}/users/status", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	String getAGS3Users(@PathVariable(value="mrktId") final String mrktId, @PathVariable(value="langCd") final String langCd,
						@RequestHeader("devkey") String devKeyHdr, @RequestHeader("X-Sec-Token") String token, @RequestHeader("acctNr") String acctNr,
						@RequestParam String view, @RequestParam String status) {
    	
    	System.out.println("\n PUBLIC getAGS3Users: " + mrktId + "/" + langCd + "\n");

    	String uri = "http://webeservices3qaf.avon.com/ags-profile-web/rest/v1";
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		builder.pathSegment(mrktId, langCd, "users", "status");
		builder.queryParam("view", view);
		builder.queryParam("status", status);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("devkey", devKeyHdr);
		headers.set("X-Sec-Token", token);
		headers.set("acctNr", acctNr);
		HttpEntity<String> entity = new HttpEntity<String>("", headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.GET, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }
    
    @RequestMapping(value="/ags3/ags-lookup-web/rest/v1/{mrktId}/{langCd}/code", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	String getAGS3Code(@PathVariable(value="mrktId") final String mrktId, @PathVariable(value="langCd") final String langCd, @RequestHeader("devkey") String devKeyHdr,
						@RequestParam String entityDescTxt, @RequestParam String attributePhysicalName) {
    	
    	
    	System.out.println("\n PUBLIC postAGS3Code: " + mrktId + "/" + langCd + "\n");

    	String uri = "http://webeservices3qaf.avon.com/ags-lookup-web/rest/v1";
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		builder.pathSegment(mrktId, langCd, "code");
		builder.queryParam("entityDescTxt", entityDescTxt);
		builder.queryParam("attributePhysicalName", attributePhysicalName);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("devkey", devKeyHdr);
		HttpEntity<String> entity = new HttpEntity<String>("", headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.GET, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();
    }
    
    private String getBearerToken(String authToken) {

    	String token=null;
    	
		String[] sArray = StringUtils.isEmpty(authToken) ? null:StringUtils.tokenizeToStringArray(authToken, " ");

		if( null != sArray && sArray.length == 2 && sArray[0].equalsIgnoreCase("bearer")) {
			token = sArray[1];
		}
		
		return token;
    }

    private String secureAGSBody(String token, String body) {

    	String AGSToken=null;
    	String AGSDevKey=null;
    	String updatedBody=null;
    	
		if(!StringUtils.isEmpty(token)) {

			Map<String, Object> map = null;
	        try {
	        	OAuth2AccessToken oauth2Token = tokenServices.readAccessToken(token);
	        	map = oauth2Token.getAdditionalInformation();
	        	AGSToken = (String) map.get(CustomUserDetailsService.AGSToken);
	        	AGSDevKey = (String) map.get(CustomUserDetailsService.DevKey);

	    		// replace the input AGS Token value with the respective AGS Token Value obtained from the JWT Token
	    		String[] sArray1 = StringUtils.isEmpty(body) ? null:StringUtils.tokenizeToStringArray(body, " ");
	    		for(int i=0; i < sArray1.length; i++) {
	    			if(sArray1[i].equalsIgnoreCase("\"token\"")) {
	    				if(sArray1[i+1].equalsIgnoreCase(":")) {
	    					sArray1[i+2] = "\"" + AGSToken + "\"" + ",";
	    				}
	    			} else if(sArray1[i].equalsIgnoreCase("\"devKey\"")) {
	    				if(sArray1[i+1].equalsIgnoreCase(":")) {
	    					sArray1[i+2] = "\"" + AGSDevKey + "\"" + ",";
	    				}
	    			}
	    		}

	    		updatedBody=StringUtils.arrayToDelimitedString(sArray1, "");
	        } catch (Exception e) {
	        	logger.error(e.getMessage(), e);
	        }
		} else {
			logger.error("Token Decode Exception: Invalid Token");
		}
		
		return updatedBody;
    }
	
    private String getAttribute(String attrName, String token) {

    	Object rtn=null;
    	
		if(!StringUtils.isEmpty(token)) {

			Map<String, Object> map = null;
	        try {
	        	OAuth2AccessToken oauth2Token = tokenServices.readAccessToken(token);
	        	map = oauth2Token.getAdditionalInformation();
	        	rtn = map.get(attrName);
//	        	map.forEach((k, v) -> System.out.println("Key: " + k + " Value: " + v));
	        } catch (Exception e) {
	        	logger.error(e.getMessage(), e);
	        }
		} else {
			logger.error("Token Decode Exception: Invalid Token");
		}
		return (rtn instanceof Integer ? ((Integer)rtn).toString(): (String) rtn);
    }    
}
