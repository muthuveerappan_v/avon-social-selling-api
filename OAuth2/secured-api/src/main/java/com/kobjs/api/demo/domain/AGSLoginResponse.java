package com.kobjs.api.demo.domain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AGSLoginResponse {

	protected final static Log logger = LogFactory.getLog(AGSLoginResponse.class);

	private AGSAuth auth=null;
	private String mrktCd=null;
	private String userId=null;
	private String token=null;

	

	public AGSLoginResponse() {
	}
	
	public AGSLoginResponse(AGSAuth auth) {
		this.auth=auth;
	}

	public AGSLoginResponse(String mrktCd, String userId, String token) {
		this.mrktCd=mrktCd;
		this.userId=userId;
		this.token=token;
	}
	public AGSAuth getAuth() {
		return auth;
	}

	public void setAuth(AGSAuth auth) {
		this.auth = auth;
	}

	public String getMrktCd() {
		return mrktCd;
	}

	public void setMrktCd(String mrktCd) {
		this.mrktCd = mrktCd;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
		
	public String toJson() {
		String returnVal;
		try {
			ObjectMapper mapper = new ObjectMapper();
			returnVal = mapper.writeValueAsString(this);
		} catch (Exception e) {
			logger.error(e);
			returnVal=null;
		}
		return returnVal;
		
	}
	
	public static AGSLoginResponse fromJson(String json) {
		
		AGSLoginResponse returnVal;
		try {
			ObjectMapper mapper = new ObjectMapper();
			returnVal = mapper.readValue(json, AGSLoginResponse.class);
		} catch (Exception e) {
			logger.error(e);
			returnVal=null;
		}
		return returnVal;
		
	}
}
