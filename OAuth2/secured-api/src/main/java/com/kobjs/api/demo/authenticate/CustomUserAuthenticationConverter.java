package com.kobjs.api.demo.authenticate;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.util.StringUtils;

import com.kobjs.api.demo.repository.CustomUserDetailsService;

/**
 * An {@link UserAuthenticationConverter} implementation that retrieves user details
 * from existing AVON AGS Login Services from a {@link UserDetailsService}.
 *
 * @author Rick Mohr
 */
public class CustomUserAuthenticationConverter implements UserAuthenticationConverter {

	private Collection<? extends GrantedAuthority> defaultAuthorities;

	private UserDetailsService userDetailsService;

	/**
	 * Optional {@link UserDetailsService} to use when extracting an {@link Authentication} from the incoming map.
	 * 
	 * @param userDetailsService the userDetailsService to set
	 */
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	/**
	 * Default value for authorities if an Authentication is being created and the input has no data for authorities.
	 * Note that unless this property is set, the default Authentication created by {@link #extractAuthentication(Map)}
	 * will be unauthenticated.
	 * 
	 * @param defaultAuthorities the defaultAuthorities to set. Default null.
	 */
	public void setDefaultAuthorities(String[] defaultAuthorities) {
		this.defaultAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils
				.arrayToCommaDelimitedString(defaultAuthorities));
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.oauth2.provider.token.UserAuthenticationConverter#convertUserAuthentication(org.springframework.security.core.Authentication)
	 */
	@Override
	public Map<String, ?> convertUserAuthentication(Authentication authentication) {
		Map<String, Object> response = new LinkedHashMap<String, Object>();
		response.put(USERNAME, authentication.getName());
		if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
			response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
		}
		return response;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.oauth2.provider.token.UserAuthenticationConverter#extractAuthentication(java.util.Map)
	 */
	@Override
	public Authentication extractAuthentication(Map<String, ?> map) {

		map.forEach((k, v) -> System.out.println("\nExtract Authentication - Key: " + k + " Value: " + v));
		if (map.containsKey(USERNAME)) {
			Object principal = map.get(USERNAME);
			Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
			if (userDetailsService != null) {

				UserDetails user = null;
				String userName  = (String) map.get(USERNAME);
				
				if (userDetailsService instanceof CustomUserDetailsService && !StringUtils.isEmpty(map.get(CustomUserDetailsService.AGSToken)) ) {
					
					user = ((CustomUserDetailsService) this.userDetailsService).loadByToken(userName, map);

				} else {
					user = userDetailsService.loadUserByUsername(userName);
				}
				
				authorities = user.getAuthorities();
				principal = user;
			}
			UsernamePasswordAuthenticationToken tkn = new UsernamePasswordAuthenticationToken(principal, "N/A", authorities);
			System.out.println("UserNamePasswordToken: authenticated=" + tkn.isAuthenticated());
			return tkn;
		}
		return null;
	}

	/**
	 * Extract the Authorities from the existing Token Properties map
	 * @param map
	 * @return
	 */
	private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
		if (!map.containsKey(AUTHORITIES)) {
			return defaultAuthorities;
		}
		Object authorities = map.get(AUTHORITIES);
		if (authorities instanceof String) {
			return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
		}
		if (authorities instanceof Collection) {
			return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils
					.collectionToCommaDelimitedString((Collection<?>) authorities));
		}
		throw new IllegalArgumentException("Authorities must be either a String or a Collection");
	}	
}
