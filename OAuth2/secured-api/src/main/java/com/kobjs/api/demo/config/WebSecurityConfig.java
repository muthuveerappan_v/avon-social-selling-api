package com.kobjs.api.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
		.requestMatchers().antMatchers("/agsws/**/*", "/ags3/**/*")
		.and()
		.csrf().disable()
		.authorizeRequests()
			.anyRequest().permitAll()
		.and().sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.NEVER)
		.and()
			.httpBasic();		
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		web.debug(true);
		super.configure(web);
	}


}
