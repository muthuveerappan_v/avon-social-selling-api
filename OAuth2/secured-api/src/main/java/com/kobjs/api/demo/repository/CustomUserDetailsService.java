package com.kobjs.api.demo.repository;

import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface CustomUserDetailsService {

	final String MrktCd = "mrktCd";
	final String LangCd = "langCd";
	final String AcctNr = "acctNr";
	final String AGSToken = "AGSToken";
	final String UserType = "userType";
	final String Domain = "domain";
	final String DevKey = "devKey";
	final String Version = "version";
	final String ClientId = "clientId";
	
	/**
	 * Custom UserDetailsService to allow additional parameters from the request to be passed into the find by user API
	 * @param username
	 * @param password
	 * @param parameters
	 * @return
	 * @throws UsernameNotFoundException
	 */
	public UserDetails loadUserByUsername(String username, String password, Map<String, String> parameters) throws UsernameNotFoundException;
	
	/**
	 * Retrieves a refreshed AGSToken and related User Details attributes using the legacy AGS refresh token APIs.
	 * @param username
	 * @param parameters
	 * @return
	 * @throws UsernameNotFoundException
	 */
	public UserDetails loadByToken(String username, Map<String, ?> parameters) throws UsernameNotFoundException;
	
}
