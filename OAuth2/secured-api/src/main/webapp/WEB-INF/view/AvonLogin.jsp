<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>Sign In</title>
<link rel="stylesheet" type="text/css" href="css/StyleSheet.css" />
<meta name="robots" content="noindex, nofollow" />
</head>

<body>
	<form name="ssoLogin" method="post" action="perform_login">
		<div>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</div>
		<div></div>
		<div class="MainArea">
			<div class="Header">
				<span id="ctl00_PageTitleLabel">Avon Representative Sign In</span>
			</div>

			<div class="GroupXLargeMargin">
				<img src="./images/global_home_new_logo.gif" alt="logo" />
			</div>

			<div class="GroupLargeMargin">
				<div class="TextSizeXLarge">
					<span id="ctl00_STSLabel">Avon Global Services Access</span>
				</div>
			</div>
			<div class="MainActionContainer">

				<div class="GroupXLargeMargin">
					<span>Type your user name and password.</span>
				</div>
				<table class="UsernamePasswordTable">
					<tr>
						<td><span class="Label"><span>User name: </span></span></td>
						<td><input name="username"	type="text"/></td>
						<td class="TextColorSecondary TextSizeSmall"></td>
					</tr>
					<tr>
						<td><span class="Label"><span>Password: </span></span></td>
						<td><input name="password" type="password"/>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2" class="TextSizeSmall TextColorError"></td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="RightAlign GroupXLargeMargin">
								<input type="submit"
									name="submit" value="Sign In" class="Resizable" />
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>

			</div>
		</div>
	</form>
</body>
</html>