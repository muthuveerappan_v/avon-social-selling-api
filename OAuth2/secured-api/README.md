# Spring Boot OAuth2 Secure API Demo

Spring Boot - Secure API Demon Application

This application demonstrates how to secure a REST based API using the 
Oauth2 protocl.

Features:

* Secure access to all API methods using OAuth2
* Verify the OAuth2 Token
* Decode the OAuth2 Token to obtain end-user user profile details


* [Spring boot](http://projects.spring.io/spring-boot)
* Build system support
	* [Maven](http://maven.apache.org/)
* [Spring security](http://projects.spring.io/spring-security/)
* [Spring data jpa](http://projects.spring.io/spring-data-jpa/)
* [Metrics](https://github.com/dropwizard/metrics) for performance monitoring
* [Logback](http://logback.qos.ch/) for log management
* [HikariCP](https://github.com/brettwooldridge/HikariCP) for connection pooling
* [Hazelcast] (http://hazelcast.com/)
* [Thymeleaf](http://www.thymeleaf.org/) template engine

---
## Configuration
Copy the ./src/main/resources/settings.xml file to your .m2 local repository to configure the target Maven Repository for dowload of maven artifacts and deployment of build artifacts.

## Profiles

There are 7 current profiles: development, integration, qa, uat, staging, sandbox, and beta.

Each profile has a separate configuration file located under `resources/config`.  There is also a global configuration file: `application.yml` that defines properties that are available in all three application environments.

* `resources/config/application.yml`: global properties file
* `resources/config/application-dev.yml`: development properties file
* `resources/config/application-int.yml`: development properties file
* `resources/config/application-qa.yml`: development properties file
* `resources/config/application-sandbox.yml`: development properties file
* `resources/config/application-uat.yml`: development properties file
* `resources/config/application-staging.yml`: staging properties file
* `resources/config/application-beta.yml`: production properties file

---

## Development

Run the application using the Development (DEV) profile:

`mvn spring-boot:run -Dspring.profiles.active=dev`

You can access the running application at [http://localhost:8080](http://localhost:8080)


---

## Development - Remote Debugging

Run the application using the Development (DEV) profile with remote debugging enabled

`java -Xdebug  -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n -jar .\target\secure-api-demo.jar -Dspring.profiles.active=dev`

You can access the running application at [http://localhost:8080](http://localhost:8080)


---

## Integration - Cloud Environment

Run the application using the integration profile:

`mvn -Pint spring-boot:run`

Compile, test, and then package the application:

`mvn -Pint package`

Deploy the application to the integration Tomcat server:

`mvn -Pint tomcat:deploy`

---

## QA

Run the application using the qa profile:

`mvn -Pqa spring-boot:run`

Compile, test, and then package the application:

`mvn -Pqa package`

Deploy the application to the integration Tomcat server:

`mvn -Pqa tomcat:deploy`

---

## Staging

Run the application using the staging profile:

`mvn -Pstaging spring-boot:run`

Compile, test, and then package the application:

`mvn -Pstaging package`

---

## UAT

Run the application using the uat profile:

`mvn -Puat spring-boot:run`

Compile, test, and then package the application:

`mvn -Puat package`

---

## Production

Run the application using the production profile:

`mvn -Pbeta spring-boot:run `

Compile, test, and then package the application:

`mvn -Pbeta package`

---

## Testing

Run the full test suite:

`mvn test`

Run tests for a class:

`mvn -Dtest=$className test`

where `$className` is the test class to run.  For example:

`mvn -Dtest=RatingCalculatorDTOHouseholdLiveOnlyTests test`

Run a specific test in a class:

`mvn -Dtest=$className#$testName test`

where `$testName` is a method in a test.  For example:

`mvn -Dtest=RatingCalculatorDTOHouseholdLiveOnlyTests#id_should_be_valid test`

## Known issues

2015-08-06 12:54:05,488 -> [ERROR] org.springframework.boot.context.embedded.tomcat.TomcatStarter - 
Error starting Tomcat context: org.springframework.beans.factory.UnsatisfiedDependencyException
Caused by: java.text.ParseException: Unparseable date: "24-SEP-14"

Add: -Duser.language=en -Duser.country=US to the list of JVM command line arguments










