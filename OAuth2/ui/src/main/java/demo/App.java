package demo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;


@SpringBootApplication
public class App extends SpringBootServletInitializer {

	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(App.class);
		app.run(args);

		
	}


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application
            .bannerMode(Banner.Mode.CONSOLE)
            .sources(App.class);
    }
    
}