package demo;

import java.security.KeyPair;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.util.StringUtils;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableResourceServer
public class ResourceApplication {

	@RequestMapping("/")
	public Message home(@RequestHeader("Authorization") String authHdr) {

    	System.out.println(authHdr);

		String[] sArray = StringUtils.isEmpty(authHdr) ? null:StringUtils.tokenizeToStringArray(authHdr, " ");
		String token = (null != sArray && sArray.length == 2 ? sArray[1]:null);

		System.out.println(token);
		
		Message msg = null;
		if(!StringUtils.isEmpty(token)) {

			Map<String, Object> map = null;
	        try {
	        	OAuth2AccessToken oauth2Token = tokenServices().readAccessToken(token);
	        	System.out.println(oauth2Token.getTokenType());
	        	System.out.println(oauth2Token.getValue());
	        	System.out.println(oauth2Token.isExpired());
	        	map = oauth2Token.getAdditionalInformation();
	        	System.out.println(map.keySet());
	        	msg = new Message("Hello World");
	        } catch (Exception e) {
	        	msg = new Message(e.getMessage());
	        }
		} else {
			msg = new Message("Missing Authorization Header");
		}
		
		return msg;
	}

    
	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		KeyPair keyPair = new KeyStoreKeyFactory(
				new ClassPathResource("keystore.jks"), "foobar".toCharArray())
				.getKeyPair("test");
		converter.setKeyPair(keyPair);
		return converter;
	}


	@Bean
	public JwtTokenStore tokenStore() throws Exception {
		JwtAccessTokenConverter enhancer = jwtAccessTokenConverter();
//		// N.B. in a real system you would have to configure the verifierKey (or use JdbcTokenStore)
		enhancer.afterPropertiesSet();
		return new JwtTokenStore(enhancer);
	}	

	@Bean
    public ResourceServerTokenServices tokenServices() throws Exception {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }
	
	public static void main(String[] args) {
		SpringApplication.run(ResourceApplication.class, args);
	}

}

class Message {
	private String id = UUID.randomUUID().toString();
	private String content;

	Message() {
	}

	public Message(String content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}
