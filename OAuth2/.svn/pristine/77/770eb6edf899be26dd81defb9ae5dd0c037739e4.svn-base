package com.kobjs.api.demo.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.kobjs.api.demo.domain.AGSLogin;
import com.kobjs.api.demo.domain.AGSLoginRequest;
import com.kobjs.api.demo.domain.AGSLoginResponse;
import com.kobjs.api.demo.domain.UserAccount;
import com.kobjs.api.demo.repository.CustomUserDetailsService;
import com.kobjs.api.demo.repository.UserAccountRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService,
		AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	// ~ Instance fields
	// ================================================================================================

	protected final Log logger = LogFactory.getLog(getClass());

	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	// ~ Methods
	// ========================================================================================================

	/**
	 * @return the messages
	 */
	protected MessageSourceAccessor getMessages() {
		return this.messages;
	}

	@Autowired
	RestTemplate restTemplate;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("LoadByUsername: " + username);

		List<UserAccount> users = userAccountRepository.loadUsersByUsername(username);
		if (users.size() == 0) {
			this.logger.debug("Query returned no results for user '" + username + "'");

			throw new UsernameNotFoundException(this.messages.getMessage("JdbcDaoImpl.notFound",
					new Object[] { username }, "Username {0} not found"));
		}

		return (UserDetails) users.get(0); // contains no GrantedAuthority[]
	}

	// @Override
	public UserDetails loadUserByUsername(String username, String password, Map<String, String> parameters)
			throws UsernameNotFoundException {

		UserDetails user = null;

		System.out.println("Custom LoadByUsername: " + username + "/" + password);

		// String uri = "http://gturnquist-quoters.cfapps.io/api/random";

		// AGSLogin login = new AGSLogin("web", "66015535", "123abc",
		// "Fy9s3PCOW4vQoW/+EXSq8RpUSRXO4WtB", "BR", "pt", "1");
		AGSLoginRequest req;
		UriComponentsBuilder builder;
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity;

		if (!StringUtils.isEmpty(parameters.get("userType")) && !StringUtils.isEmpty(parameters.get("domain"))) {
			String ags3Uri = "http://webeservices3qaf.avon.com/ags-auth-web/rest/v1/";
			req = new AGSLoginRequest(username, password, parameters.get("userType"), parameters.get("domain"));
			builder = UriComponentsBuilder.fromHttpUrl(ags3Uri);
			builder.pathSegment(parameters.get("mrktCd"), parameters.get("langCd"), "login");
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.set("devkey", parameters.get("devKey"));
			System.out.println(req.toJson());
			entity = new HttpEntity<String>(req.toJson(), headers);

		} else {
			String classicUri = "http://webeservicesqaf.avon.com/agws/srvc/login";
			AGSLogin login = new AGSLogin(parameters.get("clientId"), username, password, parameters.get("devKey"),
					parameters.get("mrktCd"), parameters.get("langCd"), parameters.get("version"));
			builder = UriComponentsBuilder.fromHttpUrl(classicUri);
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			req = new AGSLoginRequest(login);
			System.out.println(req.toJson());
			entity = new HttpEntity<String>(req.toJson(), headers);
		}

		ResponseEntity<AGSLoginResponse> result = restTemplate.exchange(builder.build().encode().toUri().toString(),
				HttpMethod.POST, entity, AGSLoginResponse.class);

		if (result.getStatusCode().is2xxSuccessful()) {

			System.out.println(result.getBody().toJson());

			AGSLoginResponse res = result.getBody();

			// Spring Security is expecting this API to return the encoded
			// password for the User returned from loadByUsername
			// The returned password text is encoded, and used by Spring
			// Security to authenticated the entered password.
			// Since, the AGS API login call authenticates the username/password
			// for us, but does not return the encoded pwd,
			// we can assume the passed clear text password is valid, encoded
			// and pass back as part of the UserDetails object
			// to allow Spring to authenticate. If the AGS Login call fails,
			// then an UsernameNotFoundException is thrown
			// forcing Spring Security to reject the authentication request.
			// CustomMd5PasswordEncoder encoder = new
			// CustomMd5PasswordEncoder();
			String encodePwd = passwordEncoder.encodePassword(password, null);
			System.out.println("encodedPwd: " + encodePwd);

			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

			user = new UserAccount(null != res.getAuth() ? res.getAuth().getUserId() : res.getUserId(), encodePwd,
					authorities, null != res.getAuth() ? res.getAuth().getMrktCd() : res.getMrktCd(),
					null != res.getAuth() ? res.getAuth().getUserId() : res.getUserId(),
					null != res.getAuth() ? res.getAuth().getAcctNr() : null, "ROLE_USER",
					null != res.getAuth() ? res.getAuth().getToken() : res.getToken(),
					null != res.getAuth() ? null : parameters.get("userType"),
					null != res.getAuth() ? null : parameters.get("domain"), true, true, true, true);
		} else {
			throw new UsernameNotFoundException(result.getStatusCode() + "- Success="
					+ result.getBody().getAuth().getSuccess() + " Code=" + result.getBody().getAuth().getCode());
		}

		// ResponseEntity<String> result =
		// restTemplate.exchange(builder.build().encode().toUri().toString(),
		// HttpMethod.GET, entity, String.class);
		System.out.println(user);

		// System.out.println("Mapper: " + user.toString());
		return user;

	}

	@Override
	public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
		System.out.println(this.getClass().getName() + " loading....");
		Object principal = token.getPrincipal();
		if (principal instanceof com.kobjs.api.demo.domain.UserAccount) {

			com.kobjs.api.demo.domain.UserAccount user = (com.kobjs.api.demo.domain.UserAccount) principal;

			System.out.println("Refresh for User: " + user.getUsername() + " token: " + user.getAGSToken());
		}

		return null;
	}
}