package com.avon.security.oauth2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AGSLogin {

	private String clientId;
	private String userId;
	private String password;
	private String devKey;
	private String mrktCd;
	private String langCd;
	private String version;

	public AGSLogin() {
		
	}
	
	public AGSLogin( String clientId, String userId, String password, String devKey, String mrktCd, String langCd, String version) {
		this.clientId=clientId;
		this.userId=userId;
		this.password=password;
		this.devKey=devKey;
		this.mrktCd=mrktCd;
		this.langCd=langCd;
		this.version=version;
		
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDevKey() {
		return devKey;
	}

	public void setDevKey(String devKey) {
		this.devKey = devKey;
	}

	public String getMrktCd() {
		return mrktCd;
	}

	public void setMrktCd(String mrktCd) {
		this.mrktCd = mrktCd;
	}

	public String getLangCd() {
		return langCd;
	}

	public void setLangCd(String langCd) {
		this.langCd = langCd;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
}