package com.avon.security.oauth2.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

public class ClientAccount implements Serializable, ClientDetails {

	private static final long serialVersionUID = -8594649320134445450L;

	private Collection<GrantedAuthority> authorities;
	private Integer accessTokenValiditySeconds;
	private Integer refreshTokenValiditySeconds;
	private Map<String, Object> additionalInformation;
	private String clientId;
	private String clientSecret;
	private Set<String> registeredRedirectUri;
	private Set<String> grantTypes;
	private Set<String> resourceIds;
	private Set<String> scope;
	private boolean isAutoApprove = false;
	private boolean isScoped = false;
	private boolean IsSecretRequired = false;

	public ClientAccount(String clientId, String clientSecret, Set<String> registeredRedirectUri,
			Collection<GrantedAuthority> authorities, Integer accessTokenValiditySeconds,
			Integer refreshTokenValiditySeconds, Map<String, Object> additionalInformation, Set<String> grantTypes,
			Set<String> resourceIds, Set<String> scope, boolean isAutoApprove, boolean isScoped,
			boolean IsSecretRequired) {

		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.registeredRedirectUri = registeredRedirectUri;
		this.authorities = authorities;
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
		this.additionalInformation = additionalInformation;
		this.grantTypes = grantTypes;
		this.resourceIds = resourceIds;
		this.scope = scope;
		this.isAutoApprove = isAutoApprove;
		this.isScoped = isScoped;
		this.IsSecretRequired = IsSecretRequired;

	}

	@Override
	public String getClientId() {
		return this.clientId;
	}

	@Override
	public String getClientSecret() {
		return clientSecret;
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		return this.registeredRedirectUri;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return this.accessTokenValiditySeconds;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return this.refreshTokenValiditySeconds;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		return this.additionalInformation;
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return this.grantTypes;
	}

	@Override
	public Set<String> getResourceIds() {
		return this.resourceIds;
	}

	@Override
	public Set<String> getScope() {
		return this.scope;
	}

	@Override
	public boolean isAutoApprove(String arg0) {
		return this.isAutoApprove;
	}

	@Override
	public boolean isScoped() {
		return this.isScoped;
	}

	@Override
	public boolean isSecretRequired() {
		return this.IsSecretRequired;
	}

	@Override
	public String toString() {
		return "ClientId=" + this.clientId + ", Client Secret=" + this.clientSecret + ", Registered Redirect Uri: "
				+ this.registeredRedirectUri;
	}

}
