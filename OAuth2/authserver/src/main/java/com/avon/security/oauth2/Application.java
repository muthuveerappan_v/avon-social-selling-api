package com.avon.security.oauth2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;

import com.avon.security.oauth2.web.LoggingRequestInterceptor;


@SpringBootApplication
public class Application extends SpringBootServletInitializer {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");		

		SpringApplication.run(Application.class, args);
	}

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.bannerMode(Banner.Mode.CONSOLE);
    }
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {

		RestTemplate rt = builder.build();

		//Configure HTTP Request Logger Intercepter		
		ClientHttpRequestInterceptor ri = new LoggingRequestInterceptor();
		List<ClientHttpRequestInterceptor> ris = new ArrayList<ClientHttpRequestInterceptor>();
		ris.add(ri);
		rt.setInterceptors(ris);
		
		return rt;
	}
	
	/**
     * Initialize Application
     * @throws IOException
     */
    @PostConstruct
    public void initApplication() throws IOException {

//    	List<User> users = loginService.getUsers();
//    	users.forEach(user->System.out.println(user));
    }
}
