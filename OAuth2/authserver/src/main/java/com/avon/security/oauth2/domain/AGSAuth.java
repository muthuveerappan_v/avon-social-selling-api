package com.avon.security.oauth2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AGSAuth {

	private String mrktCd;
	private String userId;
	private Long acctNr;
	private String token;
	private String success;
	private String code;
	private String elapsedTime;
	private String timestamp;

	public AGSAuth() {
		
	}
	
	public AGSAuth( String mrktCd, String userId, Long acctNr, String token, String success, String code, String elapsedTime, String timestamp) {
		this.mrktCd=mrktCd;
		this.userId=userId;
		this.acctNr=acctNr;
		this.token=token;
		this.success=success;
		this.code=code;
		this.elapsedTime=elapsedTime;
		this.timestamp=timestamp;		
	}

	public String getMrktCd() {
		return mrktCd;
	}

	public void setMrktCd(String mrktCd) {
		this.mrktCd = mrktCd;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getAcctNr() {
		return acctNr;
	}

	public void setAcctNr(Long acctNr) {
		this.acctNr = acctNr;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


}