package com.avon.security.oauth2.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.avon.security.oauth2.domain.AGSLogin;
import com.avon.security.oauth2.domain.AGSLoginRequest;
import com.avon.security.oauth2.domain.AGSLoginResponse;
import com.avon.security.oauth2.domain.UserAccount;
import com.avon.security.oauth2.repository.UserAccountRepository;
import com.avon.security.oauth2.service.CustomUserDetailsService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService {

	// ~ Instance fields
	// ================================================================================================

	protected final Log logger = LogFactory.getLog(getClass());

	private final String Login = "login";
	private final String Renew = "renew";
	private final String SecToken = "X-Sec-Token";
	
	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	// ~ Methods
	// ========================================================================================================

	/**
	 * @return the messages
	 */
	protected MessageSourceAccessor getMessages() {
		return this.messages;
	}

	@Autowired
	RestTemplate restTemplate;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("LoadByUsername: " + username);

		List<UserAccount> users = userAccountRepository.loadUsersByUsername(username);
		if (users.size() == 0) {
			this.logger.debug("Query returned no results for user '" + username + "'");

			throw new UsernameNotFoundException(this.messages.getMessage("JdbcDaoImpl.notFound",
					new Object[] { username }, "Username {0} not found"));
		}

		return (UserDetails) users.get(0); // contains no GrantedAuthority[]
	}

	@Override
	public UserDetails loadUserByUsername(String username, String password, Map<String, String> parameters)
			throws UsernameNotFoundException {

		UserDetails user = null;

		System.out.println("Custom LoadByUsername: " + username + "/" + password);

		AGSLoginRequest req;
		UriComponentsBuilder builder;
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity;

		if (!StringUtils.isEmpty(parameters.get(CustomUserDetailsService.UserType)) && !StringUtils.isEmpty(parameters.get(CustomUserDetailsService.Domain))) {
			String ags3Uri = "http://webeservices3qaf.avon.com/ags-auth-web/rest/v1/";
			req = new AGSLoginRequest(username, password, parameters.get(CustomUserDetailsService.UserType), parameters.get(CustomUserDetailsService.Domain));
			builder = UriComponentsBuilder.fromHttpUrl(ags3Uri);
			builder.pathSegment(parameters.get(MrktCd), parameters.get(CustomUserDetailsService.LangCd), Login);
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.set(CustomUserDetailsService.DevKey, parameters.get(CustomUserDetailsService.DevKey));
			System.out.println(req.toJson());
			entity = new HttpEntity<String>(req.toJson(), headers);

		} else {
			String classicUri = "http://webeservicesqaf.avon.com/agws/srvc/login";
			AGSLogin login = new AGSLogin(parameters.get("clientId"), username, password, parameters.get(CustomUserDetailsService.DevKey),
					parameters.get(CustomUserDetailsService.MrktCd), parameters.get(CustomUserDetailsService.LangCd), parameters.get(CustomUserDetailsService.Version));
			builder = UriComponentsBuilder.fromHttpUrl(classicUri);
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			req = new AGSLoginRequest(login);
			System.out.println(req.toJson());
			entity = new HttpEntity<String>(req.toJson(), headers);
		}

		ResponseEntity<AGSLoginResponse> result = restTemplate.exchange(builder.build().encode().toUri().toString(),
				HttpMethod.POST, entity, AGSLoginResponse.class);

		if (result.getStatusCode().is2xxSuccessful()) {

			System.out.println(result.getBody().toJson());

			AGSLoginResponse res = result.getBody();

			// Spring Security is expecting this API to return the encoded
			// password for the User returned from loadByUsername
			// The returned password text is encoded, and used by Spring
			// Security to authenticated the entered password.
			// Since, the AGS API login call authenticates the username/password
			// for us, but does not return the encoded pwd,
			// we can assume the passed clear text password is valid, encoded
			// and pass back as part of the UserDetails object
			// to allow Spring to authenticate. If the AGS Login call fails,
			// then an UsernameNotFoundException is thrown
			// forcing Spring Security to reject the authentication request.
			// CustomMd5PasswordEncoder encoder = new
			// CustomMd5PasswordEncoder();
			String encodePwd = passwordEncoder.encodePassword(password, null);
			System.out.println("encodedPwd: " + encodePwd);

			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

			// AGS Nova does not return Langauge Code, but it is required for future requests, therefore, we add it to the Response using the 
			// original LangCd from the request			
			user = new UserAccount(null != res.getAuth() ? res.getAuth().getUserId() : res.getUserId(), encodePwd,
					authorities, null != res.getAuth() ? res.getAuth().getMrktCd() : res.getMrktCd(), (String) parameters.get(CustomUserDetailsService.LangCd), (String) parameters.get(DevKey),
					null != res.getAuth() ? res.getAuth().getUserId() : res.getUserId(),
					null != res.getAuth() ? res.getAuth().getAcctNr() : NumberUtils.parseNumber(username, Long.class),
					"ROLE_USER",
					null != res.getAuth() ? res.getAuth().getToken() : res.getToken(),
					null != res.getAuth() ? null : parameters.get(CustomUserDetailsService.UserType),
					null != res.getAuth() ? null : parameters.get(CustomUserDetailsService.Domain), true, true, true, true);
		} else {
			throw new UsernameNotFoundException(result.getStatusCode() + "- Success="
					+ result.getBody().getAuth().getSuccess() + " Code=" + result.getBody().getAuth().getCode());
		}

		System.out.println(user);

		return user;

	}


	/* (non-Javadoc)
	 * @see com.kobjs.api.demo.repository.CustomUserDetailsService#loadByToken(java.lang.String, java.util.Map)
	 */
	@Override
	public UserDetails loadByToken(String username, Map<String, ?> parameters) throws UsernameNotFoundException {
		UserDetails user = null;

		System.out.println("Custom LoadByToken: " + username + "/" + parameters.get(AGSToken));

		UriComponentsBuilder builder;
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity;

		String ags3Uri = "http://webeservices3qaf.avon.com/ags-auth-web/rest/v1/";
		builder = UriComponentsBuilder.fromHttpUrl(ags3Uri);
		builder.pathSegment((String)parameters.get(CustomUserDetailsService.MrktCd), (String)parameters.get(CustomUserDetailsService.LangCd), Login, Renew);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set(CustomUserDetailsService.DevKey, (String) parameters.get(CustomUserDetailsService.DevKey));
		headers.set(SecToken, (String)parameters.get(CustomUserDetailsService.AGSToken));
		headers.set(CustomUserDetailsService.AcctNr, null != parameters.get(CustomUserDetailsService.AcctNr) ? ((Integer)parameters.get(CustomUserDetailsService.AcctNr)).toString():username);
		if(!StringUtils.isEmpty(parameters.get(CustomUserDetailsService.UserType))) {
			headers.set(CustomUserDetailsService.UserType, null != parameters.get(CustomUserDetailsService.UserType) ? (String) parameters.get(CustomUserDetailsService.UserType):"");
		}

		entity = new HttpEntity<String>("", headers);

		ResponseEntity<AGSLoginResponse> result = restTemplate.exchange(builder.build().encode().toUri().toString(),
				HttpMethod.POST, entity, AGSLoginResponse.class);

		if (result.getStatusCode().is2xxSuccessful()) {

			System.out.println(result.getBody().toJson());

			AGSLoginResponse res = result.getBody();

			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

			user = new UserAccount(null != res.getAuth() ? res.getAuth().getUserId() : res.getUserId(), null,
					authorities, null != res.getAuth() ? res.getAuth().getMrktCd() : res.getMrktCd(), (String) parameters.get(LangCd), (String) parameters.get(DevKey),
					null != res.getAuth() ? res.getAuth().getUserId() :  res.getUserId(),
					null != res.getAuth() ? res.getAuth().getAcctNr() : NumberUtils.parseNumber(username, Long.class),
				   "ROLE_USER",
					null != res.getAuth() ? res.getAuth().getToken() : res.getToken(),
					null != res.getAuth() ? null : (String)parameters.get(CustomUserDetailsService.UserType),
					null != res.getAuth() ? null : (String)parameters.get(CustomUserDetailsService.Domain), true, true, true, true);
		} else {
			throw new UsernameNotFoundException(result.getStatusCode() + "- Success="
					+ result.getBody().getAuth().getSuccess() + " Code=" + result.getBody().getAuth().getCode());
		}

		System.out.println(user);

		return user;
		
	}

}