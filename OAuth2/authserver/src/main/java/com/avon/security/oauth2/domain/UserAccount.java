package com.avon.security.oauth2.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccount implements Serializable, UserDetails  {

    private static final long serialVersionUID = -8594649320134445450L;
    
    private Collection<GrantedAuthority> authorities;    
    private String mrktCd;
    private String userId;
    private String langCd;
    private Long acctNr;
    private String devKey;
    private String username;
    private String password;
	private String role;
	private String AGSToken;
	private String userType;
	private String domain;
	private boolean isAccountNonExpired;
	private boolean isAccountNonLocked;
	private boolean isCredentialsNonExpired;
	private boolean isEnabled;
	
	public UserAccount(String username, String password, Collection<GrantedAuthority> authorities, String mrktId, String langCd, String devKey,
			String userId, Long acctNr, String role, boolean isAccountNonExpired, boolean isAccountNonLocked,
			boolean isCredentialsNonExpired, boolean isEnabled) {

		this.username = username;
		this.password = password;
		this.authorities = authorities;
		this.mrktCd = mrktId;
		this.langCd=langCd;
		this.devKey=devKey;
		this.userId = userId;
		this.acctNr = acctNr;
		this.role = role;
		this.AGSToken = null;
		this.userType = null;
		this.domain = null;
		this.isAccountNonExpired = isAccountNonExpired;
		this.isAccountNonLocked = isAccountNonLocked;
		this.isCredentialsNonExpired = isCredentialsNonExpired;
		this.isEnabled = isEnabled;

	}
	

	public UserAccount(String username,
				String password,
				Collection<GrantedAuthority> authorities,
				String mrktId,
				String langCd,
				String devKey,
				String userId,
				Long acctNr,
				String role,
				String token,
				String userType,
				String domain,
				boolean isAccountNonExpired,
				boolean isAccountNonLocked,
				boolean isCredentialsNonExpired,
				boolean isEnabled) {
		
		this.username = username;
		this.password = password;
		this.authorities = authorities;
		this.mrktCd = mrktId;
		this.langCd=langCd;
		this.devKey=devKey;
		this.userId = userId;
		this.acctNr = acctNr;
		this.role = role;
		this.AGSToken=token;
		this.userType=userType;
		this.domain=domain;
		this.isAccountNonExpired = isAccountNonExpired;
		this.isAccountNonLocked = isAccountNonLocked;
		this.isCredentialsNonExpired = isCredentialsNonExpired;
		this.isEnabled = isEnabled;
		
	}	
    
      
	public String getMrktCd() {
        return mrktCd;
    }

	public String getLangCd() {
		return langCd;
	}

    public String getUserId() {
        return userId;
    }

    public String getDevKey() {
        return devKey;
    }
    
    public Long getAcctNr() {
        return acctNr;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getRole() {
        return role;
    }

    public String getAGSToken() {
        return AGSToken;
    }
    
    public String getUserType() {
        return userType;
    }

    public String getDomain() {
        return domain;
    }
    //  @Override
//	public String toString() {
//		return "Username=" + username + ", Password=" + password;
//	}
    
    @Override
	public String toString() {
		return "Market: " + mrktCd + ", User Id=" + userId + ", AcctNr: " + acctNr + ", Password: " + password + ", Token: " + AGSToken + ", UserType: " + userType + ", Domain: " + domain;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return this.authorities;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return this.isAccountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.isAccountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.isCredentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.isEnabled;
	}
       
}
