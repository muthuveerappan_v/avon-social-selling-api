package com.avon.security.oauth2.repository;

@SuppressWarnings("serial")
public class UsernameAlreadyInUseException extends Exception {

	public UsernameAlreadyInUseException(String username) {
		super("The username '" + username + "' is already in use.");
	}
}
