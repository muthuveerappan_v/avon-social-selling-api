package com.avon.security.oauth2.domain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AGSLoginRequest {

	protected final Log logger = LogFactory.getLog(getClass());

	private AGSLogin login;
	private String userId;
	private String password;
	private String userType;
	private String domain;


	public AGSLoginRequest(AGSLogin login) {
		this.login=login;
	}

	public AGSLogin getLogin() {
		return login;
	}

	public AGSLoginRequest( String userId, String password, String userType, String domain) {
		this.userId=userId;
		this.password=password;
		this.userType=userType;
		this.domain=domain;
		
	}
	
	public void setLogin(AGSLogin login) {
		this.login = login;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public String toJson() {
		String returnVal;
		try {
			ObjectMapper mapper = new ObjectMapper();
			returnVal = mapper.writeValueAsString(this);
		} catch (Exception e) {
			logger.error(e);
			returnVal=null;
		}
		return returnVal;
		
	}	
}
