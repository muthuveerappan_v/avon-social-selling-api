package com.avon.security.oauth2.api;

import java.security.Principal;
import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.avon.security.oauth2.domain.UserAccount;
import com.avon.security.oauth2.service.CustomUserDetailsService;
import com.avon.security.oauth2.service.impl.UserDetailsServiceImpl;

@RestController
@EnableResourceServer
public class UserProfileController {

	private final String Users = "users";
	private final String Status = "status";
	private final String View = "view";
	private final String SecToken = "X-Sec-Token";
	
	@Autowired
	UserDetailsServiceImpl loginService;
  
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
    @Qualifier("tokenServices")
	DefaultTokenServices tokenServices;
	
	@RequestMapping(value="/api/user", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_USER")
	@ResponseBody
	UserAccount getUser(@RequestParam("username") String username) {

		return  (UserAccount)  loginService.loadUserByUsername(username);
	}

	@RequestMapping(value="/api/profile/get", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_USER")
	@ResponseBody
	String postAGSClassicRepProfile(@RequestHeader("Authorization") String authHdr, @RequestBody String body) {
		
    	System.out.println("\n postAGSClassicRepProfile Authoriztion: " + authHdr + "\n");

    	String uri = "http://webeservicesqaf.avon.com/agws/srvc/repProfile/get";
    	
		String[] sArray = StringUtils.isEmpty(authHdr) ? null:StringUtils.tokenizeToStringArray(authHdr, " ");

		if( null != sArray && sArray.length == 2) {
			String tokenType = sArray[0];
			String token = sArray[1];

	    	System.out.println("\n getRepProfile TokenType: " + tokenType + " Token: " + token + "\n");

			if(!StringUtils.isEmpty(token)) {

				Map<String, Object> map = null;
		        try {
		        	OAuth2AccessToken oauth2Token = tokenServices.readAccessToken(token);
		        	System.out.println(oauth2Token.getTokenType());
		        	System.out.println(oauth2Token.getValue());
		        	System.out.println(oauth2Token.isExpired());
		        	map = oauth2Token.getAdditionalInformation();
		        	System.out.println("\nToken Decode:");
		        	map.forEach((k, v) -> System.out.println("Key: " + k + " Value: " + v));
		        	System.out.println("\n");
		        } catch (Exception e) {
		        	System.out.println("Token Decode Exception: " + e.getMessage());
		        }
			} else {
				System.out.println("Token Decode Exception: Invalid Token");
			}
			
		}
		
//	    RestTemplate restTemplate = new RestTemplate();
//	    restTemplate.g
//	    String result = restTemplate.getForObject(uri, String.class);
	     		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.POST, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();

	}

	@RequestMapping(value="/api/ags-profile-web/rest/v1/{mrktCd}/{langCd}/users/status", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured("ROLE_USER")
	@ResponseBody
	String getAGS3RepProfile(@RequestHeader(value="Authorization", required=true) String authHdr,
							 @RequestHeader(value="acctNr", required=true) String acctNr,
							 @PathVariable("mrktCd") String mrktCd,
							 @PathVariable("langCd") String langCd,
							 @RequestParam(value="view", required=true) String view,
							 @RequestParam(value="status", required=true) String status) {
		
    	System.out.println("\n getAGS3RepProfile Authoriztion: " + authHdr + "\n");

    	String uri = "http://webeservices3qaf.avon.com:80/ags-profile-web/rest/v1/";
    	
		String[] sArray = StringUtils.isEmpty(authHdr) ? null:StringUtils.tokenizeToStringArray(authHdr, " ");

		String AGSDevKey=null;
		String AGSToken=null;
		
		if( null != sArray && sArray.length == 2) {
			String tokenType = sArray[0];
			String token = sArray[1];

	    	System.out.println("\n getRepProfile TokenType: " + tokenType + " Token: " + token + "\n");

			if(!StringUtils.isEmpty(token)) {

				Map<String, Object> map = null;
		        try {
		        	OAuth2AccessToken oauth2Token = tokenServices.readAccessToken(token);
		        	System.out.println(oauth2Token.getTokenType());
		        	System.out.println(oauth2Token.getValue());
		        	System.out.println(oauth2Token.isExpired());
		        	map = oauth2Token.getAdditionalInformation();
		        	AGSDevKey=(String) map.get(CustomUserDetailsService.DevKey);
		        	AGSToken=(String) map.get(CustomUserDetailsService.AGSToken);
		        	System.out.println("\nToken Decode:");
		        	map.forEach((k, v) -> System.out.println("Key: " + k + " Value: " + v));
		        	System.out.println("\n");
		        } catch (Exception e) {
		        	System.out.println("Token Decode Exception: " + e.getMessage());
		        }
			} else {
				System.out.println("Token Decode Exception: Invalid Token");
			}
			
		}
		
	     		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
		builder.pathSegment(mrktCd, langCd, Users, Status);
		if(!StringUtils.isEmpty(view)) {
			builder.queryParam(View, view);
		}
		if(!StringUtils.isEmpty(status)) {
			builder.queryParam(Status, status);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set(CustomUserDetailsService.DevKey, AGSDevKey);
		headers.set(SecToken, AGSToken);
		headers.set(CustomUserDetailsService.AcctNr, acctNr);

		HttpEntity<String> entity = new HttpEntity<String>("", headers);

		ResponseEntity<String> result = restTemplate.exchange(builder.build().encode().toUri().toString(),	HttpMethod.GET, entity, String.class);

		System.out.println(result.getBody());
		
		return result.getBody();

	}
	
	@RequestMapping(value="/api/admin/user", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@Secured("ROLE_ADMIN")
	@ResponseBody
	public Principal user(@RequestHeader("Authorization") String authHdr, Principal user) {
		System.out.println("Auth Code:" + authHdr);
		return user;
	}

//	@Bean
//    public ResourceServerTokenServices tokenServices() throws Exception {
//        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
//        defaultTokenServices.setTokenStore(tokenStore());
//        return defaultTokenServices;
//    }
//	
//	protected String extractHeaderToken(String token) {
//		Enumeration<String> headers = request.getHeaders("Authorization");
//		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
//			String value = headers.nextElement();
//			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
//				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
//				// Add this here for the auth details later. Would be better to change the signature of this method.
//				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
//						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
//				int commaIndex = authHeaderValue.indexOf(',');
//				if (commaIndex > 0) {
//					authHeaderValue = authHeaderValue.substring(0, commaIndex);
//				}
//				return authHeaderValue;
//			}
//		}
//
//		return null;
//	}	
}
