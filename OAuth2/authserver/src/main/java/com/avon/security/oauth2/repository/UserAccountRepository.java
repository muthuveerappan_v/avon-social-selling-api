package com.avon.security.oauth2.repository;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.stereotype.Repository;

import com.avon.security.oauth2.domain.UserAccount;

/**
 * Jdbc user management service, based on the AVON table structure as its parent
 * class, <tt>JdbcDaoImpl</tt>.
 * <p>
 * Provides CRUD operations for both users and groups. Note that if the
 * {@link #setEnableAuthorities(boolean) enableAuthorities} property is set to
 * false, calls to createUser, updateUser and deleteUser will not store the
 * authorities from the <tt>UserDetails</tt> or delete authorities for the user.
 * Since this class cannot differentiate between authorities which were loaded
 * for an individual or for a group of which the individual is a member, it's
 * important that you take this into account when using this implementation for
 * managing your users.
 *
 * @author R. Mohr, Kinetic Objects
 * @since 1.0
 */
@Repository
public class UserAccountRepository extends JdbcDaoSupport {

	// ~ Static fields/initializers
	// =====================================================================================

	// UserDetails - Authentication SQL
	public static final String DEF_USERS_BY_USERNAME_QUERY = "select u.MRKT_ID, u.USER_ID, u.ACCT_NR, u.PSWRD_TXT, u.LANG_CD"
			+ " from USER_ID u " + " where u.USER_ID = ?";
	public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY = "select username,authority " + "from authorities "
			+ "where username = ?";
	public static final String DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY = "select g.id, g.group_name, ga.authority "
			+ "from groups g, group_members gm, group_authorities ga " + "where gm.username = ? "
			+ "and g.id = ga.group_id " + "and g.id = gm.group_id";

	// UserDetails CRUD SQL
	// public static final String DEF_CREATE_USER_SQL = "insert into users
	// (username, password, enabled) values (?,?,?)";
	// public static final String DEF_DELETE_USER_SQL = "delete from users where
	// username = ?";
	// public static final String DEF_UPDATE_USER_SQL = "update users set
	// password = ?, enabled = ? where username = ?";
	// public static final String DEF_INSERT_AUTHORITY_SQL = "insert into
	// authorities (username, authority) values (?,?)";
	// public static final String DEF_DELETE_USER_AUTHORITIES_SQL = "delete from
	// authorities where username = ?";
	public static final String DEF_USER_EXISTS_SQL = "select u.USER_ID from USER_ID u where u.USER_ID = ?";
	public static final String DEF_CHANGE_PASSWORD_SQL = "update USER_ID set PSWRD_TXT = ? where USER_ID = ?";

	// ~ Instance fields
	// ================================================================================================

	protected final Log logger = LogFactory.getLog(getClass());

	// UserDetails - Authentication SQL
	private String authoritiesByUsernameQuery;
	private String groupAuthoritiesByUsernameQuery;
	private String usersByUsernameQuery;

	// UserDetails CRUD SQL
	// private String createUserSql = DEF_CREATE_USER_SQL;
	// private String deleteUserSql = DEF_DELETE_USER_SQL;
	// private String updateUserSql = DEF_UPDATE_USER_SQL;
	// private String createAuthoritySql = DEF_INSERT_AUTHORITY_SQL;
	// private String deleteUserAuthoritiesSql =
	// DEF_DELETE_USER_AUTHORITIES_SQL;
	private String userExistsSql = DEF_USER_EXISTS_SQL;
	private String changePasswordSql = DEF_CHANGE_PASSWORD_SQL;

	// Spring Security Authentication Manager
	private AuthenticationManager authenticationManager;

	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();	
	
	// ~ Constructors
	// ===================================================================================================
	@Autowired
	public UserAccountRepository(DataSource dataSource) {
		this.usersByUsernameQuery = UserAccountRepository.DEF_USERS_BY_USERNAME_QUERY;
		this.authoritiesByUsernameQuery = UserAccountRepository.DEF_AUTHORITIES_BY_USERNAME_QUERY;
		this.groupAuthoritiesByUsernameQuery = UserAccountRepository.DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY;
		this.userExistsSql = UserAccountRepository.DEF_USER_EXISTS_SQL;
		this.changePasswordSql = UserAccountRepository.DEF_CHANGE_PASSWORD_SQL;
		this.setJdbcTemplate(new JdbcTemplate(dataSource));
	}

	// ~ Methods
	// ========================================================================================================

	/**
	 * @return the messages
	 */
	protected MessageSourceAccessor getMessages() {
		return this.messages;
	}
	
	@Override
	protected void initDao() throws Exception {
		if (authenticationManager == null) {
			logger.info("No authentication manager set. Reauthentication of users when changing passwords will "
					+ "not be performed.");
		}

		super.initDao();
	}


	/**
	 * Executes the SQL <tt>usersByUsernameQuery</tt> and returns a list of
	 * UserDetails objects. There should normally only be one matching user.
	 */
	public List<UserAccount> loadUsersByUsername(String username) {
		return getJdbcTemplate().query(this.usersByUsernameQuery, new String[] { username },
				new UserAccountRowMapper());
	}
	
	// public void changePassword(String oldPassword, String newPassword) throws
	// AuthenticationException {
	// Authentication currentUser =
	// SecurityContextHolder.getContext().getAuthentication();
	//
	// if (currentUser == null) {
	// // This would indicate bad coding somewhere
	// throw new AccessDeniedException(
	// "Can't change password as no Authentication object found in context " +
	// "for current user.");
	// }
	//
	// String username = currentUser.getName();
	//
	// // If an authentication manager has been set, re-authenticate the user
	// // with the
	// // supplied password.
	// if (authenticationManager != null) {
	// logger.debug("Reauthenticating user '" + username + "' for password
	// change request.");
	//
	// authenticationManager.authenticate(new
	// UsernamePasswordAuthenticationToken(username, oldPassword));
	// } else {
	// logger.debug("No authentication manager set. Password won't be
	// re-checked.");
	// }
	//
	// logger.debug("Changing password for user '" + username + "'");
	//
	// getJdbcTemplate().update(changePasswordSql, newPassword, username);
	//
	// SecurityContextHolder.getContext().setAuthentication(createNewAuthentication(currentUser,
	// newPassword));
	//
	// userCache.removeUserFromCache(username);
	// }
	//
	// protected Authentication createNewAuthentication(Authentication
	// currentAuth, String newPassword) {
	// UserDetails user = loadUserByUsername(currentAuth.getName());
	//
	// UsernamePasswordAuthenticationToken newAuthentication = new
	// UsernamePasswordAuthenticationToken(user, null,
	// user.getAuthorities());
	// newAuthentication.setDetails(currentAuth.getDetails());
	//
	// return newAuthentication;
	// }

	public boolean userExists(String username) {
		List<String> users = getJdbcTemplate().queryForList(userExistsSql, new String[] { username }, String.class);

		if (users.size() > 1) {
			throw new IncorrectResultSizeDataAccessException("More than one user found with name '" + username + "'",
					1);
		}

		return users.size() == 1;
	}
}
