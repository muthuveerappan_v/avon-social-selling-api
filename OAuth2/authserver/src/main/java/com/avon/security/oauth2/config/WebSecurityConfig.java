package com.avon.security.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.avon.security.oauth2.authenticate.AGSAuthenticationProvider;
import com.avon.security.oauth2.authenticate.CustomMd5PasswordEncoder;
import com.avon.security.oauth2.service.impl.UserDetailsServiceImpl;
import com.avon.security.oauth2.web.CustomLogoutSuccessHandler;

@SuppressWarnings("deprecation")
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
//@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
		.requestMatchers()
			.antMatchers("/login", "/AvonLogin*", "/anonymous*", "/perform_login", "/perform_logout", "/homepage*", "/oauth/confirm_access", "/oauth/authorize")
		.and()
		.authorizeRequests()
	        .antMatchers("/anonymous*").anonymous()
	        .antMatchers("/login").permitAll()
	        .antMatchers("/AvonLogin*").permitAll()
			.anyRequest().authenticated()
		.and().sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.NEVER)
		.and()
			.formLogin()
		        .loginPage("/AvonLogin.html")
		        .loginProcessingUrl("/perform_login")
//		        .defaultSuccessUrl("/homepage.html",true)
		        .failureUrl("/login.html?error=true")
        .and()
	        .logout()
	        .logoutUrl("/perform_logout")
	        .deleteCookies("JSESSIONID")
	        .logoutSuccessHandler(logoutSuccessHandler())
		.and()
			.httpBasic();		
	}

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new CustomLogoutSuccessHandler();
    }	
	// @Override
	// protected void configure(HttpSecurity http) throws Exception {
	// // TODO Auto-generated method stub
	// super.configure(http);
	// http
	// .logout().invalidateHttpSession(true).deleteCookies("JESSIONID");
	// }

	@Autowired
	UserDetailsServiceImpl userDetailsService;

	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		web.debug(true);
		web.ignoring()
			.antMatchers("/resources/**");
		super.configure(web);
	}


	@Autowired
	private AGSAuthenticationProvider authProvider;

	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	
		// set the Avon custom password encoder and user details service
		authProvider.setPasswordEncoder(passwordEncoder());
		authProvider.setUserDetailsService(userDetailsService);

		auth
			.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder())
			.and()
			.authenticationProvider(authProvider);
	}

	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordEncoder() {
		System.out.println("Loading CustomMd5PaswordEncoder");
		return (PasswordEncoder) new CustomMd5PasswordEncoder();
	}

}
