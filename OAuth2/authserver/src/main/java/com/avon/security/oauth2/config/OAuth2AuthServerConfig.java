package com.avon.security.oauth2.config;

import java.security.KeyPair;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.avon.security.oauth2.authenticate.AGSAuthenticationProvider;
import com.avon.security.oauth2.authenticate.CustomUserAuthenticationConverter;
import com.avon.security.oauth2.service.CustomUserDetailsService;
import com.avon.security.oauth2.service.impl.UserDetailsServiceImpl;


/**
 * @author rmohr
 *
 */
@SuppressWarnings("deprecation")
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private UserDetailsServiceImpl userDetailsService;
	
    @Autowired
    @Qualifier("preAuthUserDetails")
    private AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> preAuthUserDetailsService;

    @SuppressWarnings("deprecation")
	@Autowired
    @Qualifier("passwordEncoder")
    private PasswordEncoder passwordEncoder;
    
	// Reference to Custom Authorization Server Authentication Manager		
//	@Autowired
//	private AuthenticationManager authenticationManager;

	@Autowired
	private AGSAuthenticationProvider authProvider;

	@Bean
	public AuthenticationManager authenticationManager() {
		
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder);
		
		List<AuthenticationProvider> providers = Arrays.asList(authProvider, preAuthProvider());
		AuthenticationManager mgr = new ProviderManager(providers);
		return mgr;	
	}
	
	@Bean
	public PreAuthenticatedAuthenticationProvider preAuthProvider() {
	
		PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
		provider.setPreAuthenticatedUserDetailsService(preAuthUserDetailsService);
		return provider;
	}

	
//	@SuppressWarnings("deprecation")
//	@Bean(name = "passwordEncoder")
//	public PasswordEncoder passwordEncoder() {
//		System.out.println("Loading CustomMd5PaswordEncoder");
//		return (PasswordEncoder) new CustomMd5PasswordEncoder();
//	}

//	@Autowired
//	private ClientDetailsServiceImpl clientDetailsService;
	
 	/**
 	 * Create the AVON specific JWT Access Token converter used to decorate the JWT access token
 	 * with the AVON required attributes.
 	 * 
 	 * @return
 	 */
 	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
 		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource("keystore.jks"), "foobar".toCharArray())
				.getKeyPair("test");
		converter.setKeyPair(keyPair);
		converter.setSigningKey("mySignedKey");
		
		DefaultAccessTokenConverter t = (DefaultAccessTokenConverter) converter.getAccessTokenConverter();
		t.setUserTokenConverter(userAuthenticationConverter());		
		
		return converter;
	}

	/**
	 * Configure Oauth2 Client Service Details.  This is where we customize the ClientDetailsService to 
	 * use Avon custom ClientAccount and Client schema in their Oracle ODS.
	 * 
	 * @TODO - Replace with direct access to Avon's client database table to authenticate Avon clients.
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients
			.inMemory()
				.withClient("web").secret("acmesecret")
					.authorizedGrantTypes("authorization_code", "refresh_token", "password", "client_credentials")
					.scopes("openid").redirectUris("http://localhost:8080/login")
				.and()
				.withClient("apim").secret("Avon@606")
					.authorizedGrantTypes("authorization_code", "refresh_token", "password", "client_credentials")
					.scopes("AGS").redirectUris("https://kobjs.portal.azure-api.net/docs/services/57578b1dcce5b211040ee053/console/oauth2/authorizationcode/callback");				
	}


	/**
	 * Configure Oauth2 Endpoints to use custom Avon JWT token store, and authentication maanager.
	 */	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		endpoints
			.tokenStore(tokenStore())
			.tokenEnhancer(tokenEnhancerChain())
			.tokenServices(customTokenServices())
			.authenticationManager(authenticationManager());
		
		System.out.println("\nToken Services Override: " + endpoints.isTokenServicesOverride() + "\n");

	}


	/**
	 * Customize the Spring Security Authorization Server as follows:
	 * 
	 *  - allow public access to the /oauth/token_key service
	 *  - require Client authentication for /oauth/check_token service
	 *  - force use of POST for /oauth/token service
	 */
	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer

			// only allow POST access for /oauth/token service endpoint
			.allowFormAuthenticationForClients()
			// allow public access to /oauth/token_key to get public key
			.tokenKeyAccess("permitAll()")
			// require client authentication for /oauth/check_token service
			.checkTokenAccess("isAuthenticated()");
	}

	
    /**
     * Implement custom AVON JWT token converter into the TokenStore.
     * @return
     */
    @Bean
    public JwtTokenStore tokenStore() {
        JwtTokenStore store = new JwtTokenStore(jwtAccessTokenConverter());
        return store;
    }

    
    @Bean
    public TokenEnhancerChain tokenEnhancerChain(){
        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), jwtAccessTokenConverter()));
        return tokenEnhancerChain;
    }
	
    @Bean
    @Primary
    @Qualifier("tokenServices")
    public DefaultTokenServices customTokenServices(){
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
//        defaultTokenServices.setClientDetailsService(clientDetailsService);
        defaultTokenServices.setTokenEnhancer(tokenEnhancerChain());
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setReuseRefreshToken(true);
        defaultTokenServices.setAuthenticationManager(authenticationManager());
        
        return defaultTokenServices;
    }	

    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new CustomTokenEnhancer();
    }

    @Bean
    public UserAuthenticationConverter userAuthenticationConverter() {
    	CustomUserAuthenticationConverter customAuthenticationConverter = new CustomUserAuthenticationConverter();
        customAuthenticationConverter.setUserDetailsService(userDetailsService);
        return customAuthenticationConverter;
    }    

    
    /**
     * Custom JWT Access Token Covnverter designed to extract Avon specific attributes from UserDetails and 
     * add them to the JWT token.
     * 
     * @author rmohr
     *
     */
    protected static class CustomTokenEnhancer implements TokenEnhancer {

		@Override
		public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

			Map<String, Object> info = new HashMap<String, Object>(accessToken.getAdditionalInformation());

			final Object principal = authentication.getPrincipal();
			if (principal instanceof com.avon.security.oauth2.domain.UserAccount) {

				final com.avon.security.oauth2.domain.UserAccount user = (com.avon.security.oauth2.domain.UserAccount) principal;
				info.put(CustomUserDetailsService.MrktCd, user.getMrktCd());
				info.put(CustomUserDetailsService.AcctNr, user.getAcctNr());
				info.put(CustomUserDetailsService.AGSToken, user.getAGSToken());
				info.put(CustomUserDetailsService.UserType, user.getUserType());
				info.put(CustomUserDetailsService.Domain, user.getDomain());
				info.put(CustomUserDetailsService.LangCd, user.getLangCd());
				info.put(CustomUserDetailsService.DevKey, user.getDevKey());

			}

			if (null != info && info.size() > 0) {
				((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
			}
			return accessToken;

		}

	}
}
