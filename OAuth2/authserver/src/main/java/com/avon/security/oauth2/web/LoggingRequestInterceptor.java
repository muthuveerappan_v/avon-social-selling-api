package com.avon.security.oauth2.web;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor {

	protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);

        return response;        
    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
    	System.out.println("===========================request begin================================================");
    	System.out.println(request.toString());
    	System.out.println("URI         : " + request.getURI());
    	System.out.println("Method      : " + request.getMethod());
    	System.out.println("Headers     : " + request.getHeaders() );
    	System.out.println("Request body: " + new String(body, "UTF-8"));
    	System.out.println("==========================request end================================================");
    	
    	logger.debug("===========================request begin================================================");
    	logger.debug("URI         : {}" + request.getURI());
    	logger.debug("Method      : {}" + request.getMethod());
    	logger.debug("Headers     : {}" + request.getHeaders() );
    	logger.debug("Request body: {}" + new String(body, "UTF-8"));
    	logger.debug("==========================request end================================================");

    }
    
}