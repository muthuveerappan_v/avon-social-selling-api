package com.avon.security.oauth2.service.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.avon.security.oauth2.domain.UserAccount;
import com.avon.security.oauth2.service.CustomUserDetailsService;

@Component("preAuthUserDetails")
public class CustomPreAuthUserDetailsService
		implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	private UserDetailsService userDetailsService;
	
	public CustomPreAuthUserDetailsService() {
		System.out.println("Creating Custom Pre Auth User Service");
	}

	@Autowired
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	protected UserDetailsService getUserDetailsService() {
		return userDetailsService;
	}
	
	@Override
	public final UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) {

		System.out.println(this.getClass().getName() + " loading....");
		if (token.isAuthenticated() && token.getPrincipal() instanceof UsernamePasswordAuthenticationToken) {
			System.out.println("Already Authenticated..");

			return (UserDetails) ((UsernamePasswordAuthenticationToken) token.getPrincipal()).getPrincipal();
		} else {

			System.out.println("Not Authenticated..");

			UserDetails user = (UserDetails) ((UsernamePasswordAuthenticationToken) token.getPrincipal())
					.getPrincipal();

			if (user instanceof UserAccount && null != user) {
			
				UserAccount customUser = (UserAccount) user;

				Map<String, Object> map = new LinkedHashMap<String, Object>();
				map.put(CustomUserDetailsService.AcctNr, customUser.getAcctNr());
				map.put(CustomUserDetailsService.AGSToken, customUser.getAGSToken());
				map.put(CustomUserDetailsService.DevKey, customUser.getDevKey());
				map.put(CustomUserDetailsService.Domain, customUser.getDomain());
				map.put(CustomUserDetailsService.LangCd, customUser.getLangCd());
				map.put(CustomUserDetailsService.MrktCd, customUser.getMrktCd());
				map.put(CustomUserDetailsService.UserType, customUser.getUserType());

				if (userDetailsService instanceof CustomUserDetailsService && !StringUtils.isEmpty(map.get(CustomUserDetailsService.AGSToken)) ) {
					
					user = ((CustomUserDetailsService) this.userDetailsService).loadByToken(user.getUsername(), map);

				} else {
					user = userDetailsService.loadUserByUsername(user.getUsername());
				}				
			}

			return user;
		}
	}
}