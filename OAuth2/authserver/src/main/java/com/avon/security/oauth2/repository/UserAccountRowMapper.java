package com.avon.security.oauth2.repository;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.avon.security.oauth2.domain.UserAccount;

public class UserAccountRowMapper implements RowMapper<UserAccount> {

	public UserAccount mapRow(ResultSet resultSet, int i) throws SQLException{

       Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
       authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
       authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
	        
       UserAccount user = new UserAccount(resultSet.getString("USER_ID"),
    						 resultSet.getString("PSWRD_TXT"),
    						 authorities,
    						 resultSet.getString("MRKT_ID"),
    						 resultSet.getString("LANG_CD"),
    						 null,
    						 resultSet.getString("USER_ID"),
    						 resultSet.getLong("ACCT_NR"),
    						 "ROLE_?",
    						 true,
    						 true,
    						 true,
    						 true);
    	System.out.println("Mapper: " + user.toString());
    	return user;
    }
}
