INSERT INTO users(username,password,enabled) VALUES ('priya','priya', true);
INSERT INTO users(username,password,enabled) VALUES ('naveen','naveen', true);
INSERT INTO users(username,password,enabled) VALUES ('user','a906449d5769fa7361d7ecc6aa3f6d28', true);

INSERT INTO user_roles (user_role_id, username, role) VALUES (1, 'priya', 'ROLE_USER');
INSERT INTO user_roles (user_role_id, username, role) VALUES (2, 'priya', 'ROLE_ADMIN');
INSERT INTO user_roles (user_role_id, username, role) VALUES (3, 'naveen', 'ROLE_USER');
INSERT INTO user_roles (user_role_id, username, role) VALUES (4, 'user', 'ROLE_USER');
INSERT INTO user_roles (user_role_id, username, role) VALUES (5, 'user', 'ROLE_ADMIN');

-- Avon User Ids;
INSERT INTO USER_ID (MRKT_ID,USER_ID,ACCT_NR,FLD_LVL_NR,FLD_LVL_CD,PSWRD_TXT,PSWRD_XPIRTN_DT,LOGN_ATMPT_CNT,LAST_VIST_DT,PSWRD_HINT_TXT,REMBR_ME_IND,USER_ACS_DENL_IND,USER_STUS_CD,USER_XPIRTN_DT,USER_SESSN_ACTV_TS,USER_ACS_LVL_CD,LCL2_TXT,LCL3_TXT,CREAT_USER_ID,CREAT_TS,LAST_UPDT_USER_ID,LAST_UPDT_TS,FRUM_ID,SERVC_PSWD_TXT,USER_TYP,LANG_CD,CURRNT_SESSN_ID,NEW_PSWD_RQSTD_IND,AGRMNT_IND,AGRMNT_ACPTD_DT,AGRMNT_DESC_TXT,ACCT_LOCKOUT_TS,LAST_INVLD_LOGN_ATMPT_TS,TRNR_TYP,PRCHSNG_PRFL_CD,SCRTY_QSTN_TXT,SCRTY_ANSWR_TXT,CUST_ID,EMAIL_ADDR_TXT,USER_NM,FRST_VRTL_CMNTY_VIST_DT,LAST_VRTL_CMNTY_VIST_DT,ONLN_BNKG_PIN_ID,ONLN_BNKG_PIN_ATMPT_CNT,FRST_ORD_MSG_SENT_IND,PNLTMT_VIST_DT,PSWD_RGSTRN_URL_TXT,PSWD_RGSTRN_URL_XPIRY_DT,TEMP_PSWD_IND,LAST_MAO_VIST_DT)
VALUES (4,'1001157800',1001157800,null,null,'a906449d5769fa7361d7ecc6aa3f6d28',null,0,to_date('08-FEB-16','DD-MON-RR'),'irma',null,'Y',null,null,null,'1',null,null,'WEBELBR_ETL',to_date('30-AUG-10','DD-MON-RR'),'WEBELBR_OUSR',to_date('08-FEB-16','DD-MON-RR'),null,null,'MGR','pt_BR','c4XxVK1AXAhZVMdySH_Bc_a',null,'Y',to_date('19-SEP-10','DD-MON-RR'),'Dados privados',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
