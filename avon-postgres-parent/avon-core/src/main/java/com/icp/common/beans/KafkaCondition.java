package com.icp.common.beans;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class KafkaCondition implements Condition {

	public boolean matches(ConditionContext context,
			AnnotatedTypeMetadata metadata) {
		Boolean kafkaRqrd = context.getEnvironment()
				.getProperty("icp.env.kafka.required", Boolean.class);
		return kafkaRqrd;
	}

}
