package com.icp.common.context;

public class Context {
	private String userId = null;
	private String correlationId = null;
	private String serviceUrl = null;

	public Context(String userId,String correlationId,String serviceUrl)
	{
		this.userId = userId;
		this.correlationId = correlationId;
		this.serviceUrl = serviceUrl;
	}
	public String getUserId() {
		return userId;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
}
