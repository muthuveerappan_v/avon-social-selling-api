/*
 * Copyright 2015-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.icp.common.configserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.config.YamlProcessor;
import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.StringUtils;

import com.icp.common.context.ExceptionConfig;

import lombok.Data;
import lombok.val;

/**
 * Implementation of {@link EnvironmentRepository} that is backed by MongoDB.
 * The resulting {@link Environment} is composed of property sources where the
 * {@literal application-name} is identified by the collection's {@literal name}
 * and a MongoDB document's {@literal profile} and {@literal label} values
 * represent the Spring application's {@literal profile} and {@literal label}
 * respectively. All properties must be listed under the {@literal source} key
 * of the document.
 *
 * @author Venil Noronha
 */
public class IcpEnvironmentFactory implements EnvironmentRepository {

	private static final String LABEL = "label";
	private static final String PROFILE = "profile";
	private static final String DEFAULT = "default";
	private static final String DEFAULT_PROFILE = null;
	private static final String DEFAULT_LABEL = null;

	private NamedParameterJdbcTemplate jdbcTemplate;
	private MapFlattener mapFlattener;

	public IcpEnvironmentFactory(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.mapFlattener = new MapFlattener();
	}

	public Environment findOne(String name, String profile, String label) {
		String[] profilesArr = StringUtils
				.commaDelimitedListToStringArray(profile);
		List<String> profiles = new ArrayList<String>(
				Arrays.asList(profilesArr.clone()));
		for (int i = 0; i < profiles.size(); i++) {
			if (DEFAULT.equals(profiles.get(i))) {
				profiles.set(i, DEFAULT_PROFILE);
			}
		}
		profiles.add(DEFAULT_PROFILE); // Default configuration will have 'null'
										// profile
		profiles = sortedUnique(profiles);

		List<String> labels = Arrays.asList(label, DEFAULT_LABEL); // Default
																	// configuration
																	// will have
																	// 'null'
																	// label
		labels = sortedUnique(labels);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profiles", profiles);
		parameters.put("labels", labels);
		parameters.put("appname", name);
		String query = "SELECT profile, label, propname, propvalue FROM ICPCONFIG WHERE "
				+ "(PROFILE IN (:profiles) OR PROFILE IS NULL)"
				+ " AND (LABEL IN (:labels) OR LABEL IS NULL)"
				+ "AND APPLICATIONNAME = :appname";
		// query = "SELECT * FROM ICPCONFIG";

		Environment environment;
		try {
			List<ConfigPropertySource> sources = jdbcTemplate.query(query,
					parameters, new RowMapper<ConfigPropertySource>() {
						public ConfigPropertySource mapRow(ResultSet rs,
								int index) throws SQLException {
							ConfigPropertySource configPropertySource = new ConfigPropertySource();
							configPropertySource
									.setProfile(rs.getString("profile"));
							configPropertySource
									.setLabel(rs.getString("label"));
							configPropertySource
									.setPropname(rs.getString("propname"));
							configPropertySource
									.setPropvalue(rs.getString("propvalue"));
							return configPropertySource;
						}
					});

			sortSourcesByLabel(sources, labels);
			sortSourcesByProfile(sources, profiles);
			Map<String, Map<String, Object>> collectedMap = configureAsMap(
					sources);
			environment = new Environment(name, profilesArr, label, null);
			collectedMap.forEach((key, value) -> {
				String sourceName = generateSourceName(name, key);
				Map<String, Object> flatSource = mapFlattener.flatten(value);
				PropertySource propSource = new PropertySource(sourceName,
						flatSource);
				environment.add(propSource);
			});
		} catch (Exception e) {
			throw new IllegalStateException("Cannot load environment", e);
		}

		return environment;
	}

	private Map<String, Map<String, Object>> configureAsMap(
			List<ConfigPropertySource> sources) {
		Map<String, Map<String, Object>> collectedMap = new HashMap<String, Map<String, Object>>();
		for (ConfigPropertySource configPropertySource : sources) {
			String key = configPropertySource.getProfile() + "|"
					+ configPropertySource.getLabel();
			if (!collectedMap.containsKey(key)) {
				collectedMap.put(
						configPropertySource.getProfile() + "|"
								+ configPropertySource.getLabel(),
						new HashMap<String, Object>() {
							{
								put(configPropertySource.getPropname(),
										configPropertySource.getPropvalue());
							}
						});
			} else {
				collectedMap.get(key).put(configPropertySource.getPropname(),
						configPropertySource.getPropvalue());
			}
		}
		return collectedMap;
	}

	private ArrayList<String> sortedUnique(List<String> values) {
		return new ArrayList<String>(new LinkedHashSet<String>(values));
	}

	private void sortSourcesByLabel(List<ConfigPropertySource> sources,
			final List<String> labels) {
		Collections.sort(sources, new Comparator<ConfigPropertySource>() {

			@Override
			public int compare(ConfigPropertySource s1,
					ConfigPropertySource s2) {
				int i1 = labels.indexOf(s1.getLabel());
				int i2 = labels.indexOf(s2.getLabel());
				return Integer.compare(i1, i2);
			}

		});
	}

	private void sortSourcesByProfile(List<ConfigPropertySource> sources,
			final List<String> profiles) {
		Collections.sort(sources, new Comparator<ConfigPropertySource>() {

			@Override
			public int compare(ConfigPropertySource s1,
					ConfigPropertySource s2) {
				int i1 = profiles.indexOf(s1.getProfile());
				int i2 = profiles.indexOf(s2.getProfile());
				return Integer.compare(i1, i2);
			}

		});
	}

	private String generateSourceName(String environmentName,
			String profileLabel) {
		String sourceName;
		String[] splitArr = StringUtils.split(profileLabel, "|");
		String profile = splitArr[0] != null ? splitArr[0] : DEFAULT;
		String label = splitArr[1];
		if (label != null) {
			sourceName = String.format("%s-%s-%s", environmentName, profile,
					label);
		} else {
			sourceName = String.format("%s-%s", environmentName, profile);
		}
		return sourceName;
	}

	@Data
	public static class ConfigPropertySource {
		private String profile;
		private String label;
		private String propname;
		private Object propvalue;
	}

	private static class MapFlattener extends YamlProcessor {

		public Map<String, Object> flatten(Map<String, Object> source) {
			return getFlattenedMap(source);
		}

	}

}
