package com.icp.common.messages;

public class MsgRequest extends BasicMessage {
	private Object object;

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public MsgRequest() {
		super();
	}

	public MsgRequest(String correlationId, String sessionId) {
		super(correlationId, sessionId);
	}

	public MsgRequest(String correlationId, String sessionId, Object object) {
		super(correlationId, sessionId);
		this.object = object;
	}


	@Override
	public String toString() {
		return "MiRequest {\"username\":" + object + ", \"correlationId\":" + getCorrelationId() + ", \"cessionId\":"
				+ getSessionId() + "}";
	}
}
