package com.icp.common.exception;

public class ApplicationException extends RuntimeException
{
	private static final long serialVersionUID = 6447861470751087132L;
	private String exceptionMessage = null;

	public ApplicationException(String message)
	{
		super(message);
		this.exceptionMessage = message;
	}

	@Override
	public String getMessage()
	{
		return exceptionMessage;
	}

}
