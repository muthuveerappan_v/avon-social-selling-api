package com.icp.common.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@ConfigurationProperties(prefix = "icp.datasource")
@Component
public @Data class DatasourceEnv {

	@Value("true")
	private Boolean required;
	@Value("jdbc:postgresql://localhost:5432/postgres")
	private String jdbcUrl;
	@Value("postgres")
	private String username;
	@Value("Infosys@123")
	private String password;
	@Value("org.postgresql.Driver")
	private String driverClassName;
	@Value("5")
	private Integer maxActive;
	@Value("10000")
	private Integer maxWait;
	@Value("true")
	private Boolean testOnBorrow;

}
