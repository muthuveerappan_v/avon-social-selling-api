package com.icp.common.logger;

public class CamelLogMessage extends BaseLogMessage
{
	//Attributes applicable for camel
	private String direction = null;
	private String stepId = null;
	private String stepName = null;
	private String stepPath = null;
	
	
	public CamelLogMessage(String direction, String stepId, String stepName, String stepPath)
	{
		super();
		this.direction = direction;
		this.stepId = stepId;
		this.stepName = stepName;
		this.stepPath = stepPath;
	}
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("CAMELSYSLOG->");
		sb.append(getBaseLog());
		sb.append("|direction:"+direction);
		sb.append("|stepId:"+stepId);
		sb.append("|stepName:"+stepName);
		sb.append("|stepPath:"+stepPath);
		return sb.toString();
	}
}
