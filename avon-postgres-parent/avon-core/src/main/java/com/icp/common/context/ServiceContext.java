package com.icp.common.context;

import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class ServiceContext 
{
	public static final String CORRID_HEADER_NAME = new String("icp-correlationId");
	public static InheritableThreadLocal<Context> context = new InheritableThreadLocal<Context>();
	
	public static void setContext(ServletRequest request)
	{
		String corrId = null;
		String serviceUrl = null;
		HttpServletRequest hsr = (HttpServletRequest) request;

		//Get or Generate Correlation ID
		if(hsr.getHeader(ServiceContext.CORRID_HEADER_NAME)!=null)
		{
			corrId = hsr.getHeader(ServiceContext.CORRID_HEADER_NAME);
		}
		else
		{
			corrId = UUID.randomUUID().toString();
		}

		//Get Service Url
		serviceUrl = hsr.getRequestURL().toString();
		
		context.set(new Context(null,corrId,serviceUrl));
	}
	public static void unsetContext()
	{
		context.remove();
	}
	public static String getCorrelationId()
	{
		String retValue = null;
		if(context.get() !=null)
		{
			retValue = context.get().getCorrelationId(); 
		}
		return retValue;
	}
	public static String getUserId()
	{
		String retValue = null;
		if(context.get() !=null)
		{
			retValue = context.get().getUserId(); 
		}
		return retValue;
	}
	public static String getServiceUrl()
	{
		String retValue = null;
		if(context.get() !=null)
		{
			retValue = context.get().getServiceUrl(); 
		}
		return retValue;
	}
	
}
