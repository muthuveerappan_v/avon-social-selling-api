package com.icp.common.exception;

public class SystemException extends RuntimeException
{
	private static final long serialVersionUID = 7421654354779132746L;
	private String exceptionMessage = null;

	public SystemException(String message)
	{
		super(message);
		this.exceptionMessage = message;
	}

	@Override
	public String getMessage()
	{
		return exceptionMessage;
	}
}
