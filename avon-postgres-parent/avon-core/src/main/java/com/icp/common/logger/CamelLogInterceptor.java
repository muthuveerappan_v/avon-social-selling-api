/*package com.icp.common.logger;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.ProcessorDefinition;
import org.apache.camel.processor.DelegateAsyncProcessor;
import org.apache.camel.spi.InterceptStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CamelLogInterceptor implements InterceptStrategy
{
	Logger logger = LoggerFactory.getLogger(CamelLogInterceptor.class);
	
	public Processor wrapProcessorInInterceptors(CamelContext arg0, ProcessorDefinition<?> definition, Processor target,Processor nextTarget) throws Exception 
	{
		String stepId = definition.getId();
		String stepName = definition.getShortName();
		String stepPath = getStepPath(definition);
		return new DelegateAsyncProcessor(new Processor(){
			@Override
			public void process(Exchange exchange) throws Exception 
			{
				logger.info((new CamelLogMessage(BaseLogMessage.ENTERING, stepId, stepName, stepPath)).toString());
				target.process(exchange);
				logger.info((new CamelLogMessage(BaseLogMessage.EXITING, stepId, stepName, stepPath)).toString());
			}
		}
		);
	}

	private String getStepPath(ProcessorDefinition<?> definition) 
	{
		String path = null;
		
		if(!definition.getShortName().equalsIgnoreCase("route") && definition.getParent() != null)
		{
			path = getStepPath(definition.getParent())+"/"+definition.getShortName()+"["+definition.getId()+"]";
		}
		else
		{
			path = "/"+definition.getShortName()+"["+definition.getId()+"]";
		}
		return path;
	}
}
*/