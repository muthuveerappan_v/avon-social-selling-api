package com.icp.common.exception;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.icp.common.beans.Environment;
import com.icp.common.context.ExceptionConfig;

public class ExceptionMapper {
	private JdbcTemplate jdbcTemplate;
	private Environment props;

	ErrorResponse defaultErrorResponse = null;
	Date lastUpdated = new Date();
	HashMap<String, ErrorResponse> exceptionMap = null;
	String query = null;
	long refreshInterval;

	public ExceptionMapper(Environment props, JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.props = props;

		this.refreshInterval = props.getExceptionRefreshInterval();
		defaultErrorResponse = new ErrorResponse(
				props.getDefaultExceptionStatusCode(),
				props.getDefaultExceptionErrorCode(),
				props.getDefaultExceptionErrorMessage());

		query = new String(
				"SELECT serviceName, operationName, exceptionClass, exceptionMessage, statusCode, errorCode, errorMessage from "
						+ props.getExceptionTableName()
						+ " WHERE serviceName IN ('ALL','"
						+ props.getServiceName() + "');");

		getExceptionMap(query);
	}

	private synchronized void getExceptionMap(String query) {
		List<ExceptionConfig> exceptions = jdbcTemplate.query(query,
				new BeanPropertyRowMapper<>(ExceptionConfig.class));

		for (ExceptionConfig row : exceptions) {
			String key = row.getServiceName() + "|" + row.getOperationName()
					+ "|" + row.getExceptionClass() + "|"
					+ row.getExceptionMessage();
			ErrorResponse errResponse = new ErrorResponse(row.getStatusCode(),
					row.getErrorCode(), row.getErrorMessage());
			exceptionMap.put(key, errResponse);
		}
		lastUpdated = new Date();
	}

	public ErrorResponse mapException(Throwable cause, String serviceUrl) {
		Date currentDate = new Date();
		if (((currentDate.getTime()
				- lastUpdated.getTime()) > refreshInterval)) {
			getExceptionMap(query);
		}
		ErrorResponse returnResponse = null;

		String serviceName = props.getServiceName();
		String operationName = getOperationName(serviceUrl);
		String exceptionClass = cause.getClass().getName();
		String exceptionMessage = cause.getMessage();

		String key = serviceName + "|" + operationName + "|" + exceptionClass
				+ "|" + exceptionMessage;
		returnResponse = exceptionMap.get(key);

		if (returnResponse == null) {
			key = serviceName + "|" + operationName + "|" + exceptionClass
					+ "|ALL";
			returnResponse = exceptionMap.get(key);
		}
		if (returnResponse == null) {
			key = serviceName + "|" + operationName + "|ALL|ALL";
			returnResponse = exceptionMap.get(key);
		}
		if (returnResponse == null) {
			key = serviceName + "|ALL|ALL|ALL";
			returnResponse = exceptionMap.get(key);
		}
		if (returnResponse == null) {
			key = "ALL|ALL|ALL|ALL";
			returnResponse = exceptionMap.get(key);
		}
		if (returnResponse == null) {
			returnResponse = defaultErrorResponse;
		}
		return returnResponse;
	}

	private String getOperationName(String serviceUrl) {
		String operationName = "";
		String serviceUrlTokens[] = serviceUrl.split("/");
		if (serviceUrlTokens.length > 4) {
			operationName = serviceUrlTokens[4];
		}
		return operationName;
	}
}
