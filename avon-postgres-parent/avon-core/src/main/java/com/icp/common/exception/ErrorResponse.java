package com.icp.common.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.icp.common.context.ServiceContext;

public class ErrorResponse
{
	private String errorCode;
	private String errorMessage;
	private String referenceId;
	@JsonIgnore
	private int statusCode;
	
	public ErrorResponse(int statusCode, String errorCode, String errorMessage)
	{
		setStatusCode(statusCode);
		setErrorCode(errorCode);
		setErrorMessage(errorMessage);
		setReferenceId(ServiceContext.getCorrelationId());
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
