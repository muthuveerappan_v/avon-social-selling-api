package com.icp.common.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import com.icp.common.exception.ExceptionMapper;
import com.icp.common.exception.ExceptionServletFilter;
import com.icp.common.logger.LogServletFilter;

@Configuration
public class ServletFilterConfiguration {
	@Autowired
	private Environment props;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Bean(name = "exceptionServletFilter")
	FilterRegistrationBean addExceptionFilter() {
		ExceptionMapper exceptionMapper = new ExceptionMapper(props,
				jdbcTemplate);
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new ExceptionServletFilter(exceptionMapper));
		registration.addUrlPatterns("/*");
		registration.setName("exceptionServletFilter");
		registration.setOrder(1);
		return registration;
	}

	@Bean(name = "logServletFilter")
	FilterRegistrationBean addLogFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new LogServletFilter(props.isLogPayload()));
		registration.addUrlPatterns("/*");
		registration.setName("logServletFilter");
		registration.setOrder(2);
		return registration;
	}

}