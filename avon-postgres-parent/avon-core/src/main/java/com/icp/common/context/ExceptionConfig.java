package com.icp.common.context;

import lombok.Data;

public @Data class ExceptionConfig {

	private String serviceName;
	private String operationName;
	private String exceptionClass;
	private String exceptionMessage;
	private Integer statusCode;
	private String errorCode;
	private String errorMessage;

}