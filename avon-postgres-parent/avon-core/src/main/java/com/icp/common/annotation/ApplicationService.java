package com.icp.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.icp.common.security.MethodSecurityConfig;
import com.icp.common.security.OAuth2ResourceServerConfig;

@Service
@Inherited
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
@Import({ OAuth2ResourceServerConfig.class, MethodSecurityConfig.class })
public @interface ApplicationService {

}
