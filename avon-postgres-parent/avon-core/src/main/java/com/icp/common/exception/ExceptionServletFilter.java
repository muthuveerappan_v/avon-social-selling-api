package com.icp.common.exception;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icp.common.context.ServiceContext;

public class ExceptionServletFilter implements Filter {


	private ExceptionMapper exceptionMapper = null;

	Logger logger = LoggerFactory.getLogger(ExceptionServletFilter.class);

	public ExceptionServletFilter(ExceptionMapper exceptionMapper)
	{
		this.exceptionMapper = exceptionMapper;
	}

	public void init(FilterConfig filterConfig) throws ServletException
	{
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{
		/********** Setting thread-local object for service context ***************/
		ServiceContext.setContext(request);
		
		try
		{
			chain.doFilter(request, response);
		}
		catch(Exception e)
		{
			ObjectMapper mapper = new ObjectMapper();
			byte[] errResponse = null;
			Throwable cause = e.getCause();

			ErrorResponse err = exceptionMapper.mapException(cause,ServiceContext.getServiceUrl());
			err.setReferenceId(ServiceContext.getCorrelationId());
			
			if(response instanceof HttpServletResponse)
			{
				HttpServletResponse hsr = (HttpServletResponse) response;
				hsr.setStatus(err.getStatusCode());
			}
			errResponse = mapper.writeValueAsBytes(err);
			logError(cause,err.getStatusCode(),errResponse);
			response.getOutputStream().write(errResponse);
		}

		/********** Un-setting thread-local object ***************/
		ServiceContext.unsetContext();
	}

	private void logError(Throwable cause, int statusCode, byte[] errResponse)
	{
		SystemErrorMessage error = new SystemErrorMessage(cause.getClass().getName(), cause.getMessage(), statusCode,new String(errResponse));
		logger.error(error.toString());
	}

	public void destroy()
	{//ignore
	}

}
