package com.icp.common.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import com.icp.common.security.CorsFilter;
import com.icp.common.security.MethodSecurityConfig;
import com.icp.common.security.OAuth2ResourceServerConfig;

@Component
// @Import({Environment.class,ServletFilterConfiguration.class})
@Import({ Environment.class, DatasourceEnv.class, CustomPooledDatsource.class, ServletFilterConfiguration.class,
		CorsFilter.class })
public class Initialize {
	@Autowired
	private Environment prop;

	@PostConstruct
	public void addKafkaLogAppenderToRoot() {
		/*
		 * if (prop.getKafkaRequired()) { Logger logger =
		 * Logger.getRootLogger(); KafkaLog4jAppender kafkaLog4jAppender = new
		 * KafkaLog4jAppender(); kafkaLog4jAppender.setName("icpKafkaAppender");
		 * kafkaLog4jAppender.setBrokerList( prop.getKafkaHost() + ":" +
		 * prop.getKafkaPort());
		 * kafkaLog4jAppender.setTopic(prop.getLogTopic());
		 * kafkaLog4jAppender.setSyncSend(true);
		 * kafkaLog4jAppender.setRequiredNumAcks(0);
		 * kafkaLog4jAppender.setRetries(0);
		 * kafkaLog4jAppender.setCompressionType("none");
		 * kafkaLog4jAppender.activateOptions();
		 * logger.addAppender(kafkaLog4jAppender); logger.addAppender(new
		 * ConsoleAppender(new PatternLayout())); logger.setLevel(Level.INFO); }
		 */
	}

	/*
	 * @PostConstruct public void configureArchaius() {
	 * ConcurrentCompositeConfiguration concurrentCompositeConfiguration = new
	 * ConcurrentCompositeConfiguration();
	 * 
	 * FixedDelayPollingScheduler serviceSpecificScheduler = new
	 * FixedDelayPollingScheduler( prop.getConfigInitialPollingDelay(),
	 * prop.getConfigPollingInterval(), prop.isConfigIgnoreDelete());
	 * CassandraConfigurationSource serviceConfigurationSource = new
	 * CassandraConfigurationSource( cassandraConfigSession,
	 * prop.getConfigTableName(), prop.getConfigPropColumnName(),
	 * prop.getConfigValueColumnName(), prop.getServiceName());
	 * DynamicConfiguration serviceConfigration = new
	 * DynamicConfiguration(serviceConfigurationSource,
	 * serviceSpecificScheduler);
	 * concurrentCompositeConfiguration.addConfiguration(serviceConfigration);
	 * 
	 * FixedDelayPollingScheduler genericSpecificScheduler = new
	 * FixedDelayPollingScheduler( prop.getConfigInitialPollingDelay(),
	 * prop.getGenericConfigPollingInterval(), prop.isConfigIgnoreDelete());
	 * CassandraConfigurationSource genericConfigurationSource = new
	 * CassandraConfigurationSource( cassandraConfigSession,
	 * prop.getConfigTableName(), prop.getConfigPropColumnName(),
	 * prop.getConfigValueColumnName(), "ALL"); DynamicConfiguration
	 * genericConfigration = new
	 * DynamicConfiguration(genericConfigurationSource,
	 * genericSpecificScheduler);
	 * concurrentCompositeConfiguration.addConfiguration(genericConfigration);
	 * 
	 * YAMLConfigurationSource yamlConfigurationSource = new
	 * YAMLConfigurationSource(prop.getConfigFileName()); if
	 * (yamlConfigurationSource.isExists()) { ConcurrentMapConfiguration
	 * yamlConfiguration = yamlConfigurationSource.getConfiguration();
	 * concurrentCompositeConfiguration.addConfiguration(yamlConfiguration); }
	 * 
	 * ConcurrentMapConfiguration systemConfiguration = new
	 * ConcurrentMapConfiguration(new SystemConfiguration());
	 * concurrentCompositeConfiguration.addConfiguration(systemConfiguration);
	 * 
	 * ConfigurationManager.install(concurrentCompositeConfiguration);
	 * ConfigJMXManager.registerConfigMbean(concurrentCompositeConfiguration); }
	 */

}
