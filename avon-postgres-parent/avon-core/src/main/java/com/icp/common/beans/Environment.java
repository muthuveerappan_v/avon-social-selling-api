package com.icp.common.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import kafka.cluster.Cluster;
import lombok.Data;

@Component
@Data
public class Environment {
	// Service Name and Version
	@Value("${icp.service.name:${spring.application.name:default-application}}")
	private String serviceName;
	@Value("${icp.service.version:${spring.application.version:1.0.0}}")
	private String serviceVersion;

	// Kafka Connection Properties
	@Value("${icp.env.kafka.required:false}")
	private Boolean kafkaRequired;
	@Value("${icp.env.kafka.host:localhost}")
	private String kafkaHost;
	@Value("${icp.env.kafka.port:9092}")
	private int kafkaPort;
	@Value("${icp.env.kafka.logTopic:logs}")
	private String logTopic;

	// Logging Framework Properties
	@Value("${icp.log.logPayload:false}")
	private boolean logPayload;

	// Configuration Framework Properties

	@Value("${icp.config.keyspace:ICP_KEYSPACE}")
	private String configKeyspace;
	@Value("${icp.config.tableName:CENTRAL_CONFIG}")
	private String configTableName;
	@Value("${icp.config.propColumnName:PROPERTY}")
	private String configPropColumnName;
	@Value("${icp.config.valueColumnName:VALUE}")
	private String configValueColumnName;

	@Value("${icp.config.genericPollingInterval:90000}")
	private int genericConfigPollingInterval;

	@Value("${icp.config.pollingInterval:30000}")
	private int configPollingInterval;

	@Value("${icp.config.initialPollingDelay:0}")
	private int configInitialPollingDelay;

	@Value("${icp.config.ignoreDeletes:false}")
	private boolean configIgnoreDelete;

	@Value("${icp.config.fileName:properties.yml}")
	private String configFileName;

	// Exception Handling Framework Properties

	@Value("${icp.exception.defaultStatusCode:500}")
	private int defaultExceptionStatusCode;
	@Value("${icp.exception.defaultErrorCode:SYS_ERR_DEF}")
	private String defaultExceptionErrorCode;
	@Value("${icp.exception.defaultErrorMessage:'System default error! Please contact technical support'}")
	private String defaultExceptionErrorMessage;
	// @Value("${icp.exception.mappingFileName:exception-mapping.properties}")
	// private String exceptionMappingFileName;
	@Value("${icp.exception.keyspace:ICP_KEYSPACE}")
	private String exceptionKeyspace;
	@Value("${icp.exception.tableName:EXCEPTION_CONFIG}")
	private String exceptionTableName;
	@Value("${icp.exception.refreshInterval:60000}")
	private long exceptionRefreshInterval;

}
