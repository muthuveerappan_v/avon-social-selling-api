package com.icp.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.icp.common.beans.Initialize;

@Target(value=ElementType.TYPE)
@Retention(value=RetentionPolicy.RUNTIME)
@Import(Initialize.class)
@Inherited
@SpringBootApplication
//@EnableDiscoveryClient
//@EnableConfigServer
public @interface Service {

}
