package com.icp.common.springconfig.server;

import org.springframework.boot.SpringApplication;

import com.icp.common.annotation.ApplicationService;
import com.icp.common.configserver.EnableIcpConfigServer;

@ApplicationService
@EnableIcpConfigServer
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}